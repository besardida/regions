import { HelpFormApplication } from "../about/help-form-application.mjs";
import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { Logger } from "../logger/logger.mjs";
import { RegionsBackupTypes, RegionsCustomTagBackup, RegionsGlobalBackup, RegionsRegionBackup, RegionsTemplateBackup } from "../models/regions-backup.mjs";
import { Regions } from "../regions.mjs";

export class RegionsBackupManagerApplication extends HelpFormApplication {
    #hookIndexes = {
        "regionsCreateCustomTag": null,
        "regionsDeleteCustomTag": null,
        "regionsUpdateCustomTags": null,
        "regionsUpdateRegion": null,
        "regionsDeleteRegion": null,
        "regionsCreateRegion": null,
        "regionsCreateTemplate": null,
        "regionsUpdateTemplate": null,
        "regionsDeleteTemplate": null,
        "closeApplication": null
    };

    constructor(object, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Backups";
        super(object, options);
    }

    static async importSampleData(reloadFoundry = false) {
        let app = new RegionsBackupManagerApplication();
        let fetchResponse = await fetch('modules/regions/assets/fvtt-regions-template-Default Region Template.json');
        let fetchBlob = await fetchResponse.blob();
        let template = JSON.parse(await fetchBlob.text());
        await app._importTemplateFile(template);

        fetchResponse = await fetch('modules/regions/assets/fvtt-regions-template-Default Major Location Template.json');
        fetchBlob = await fetchResponse.blob();
        template = JSON.parse(await fetchBlob.text());
        await app._importTemplateFile(template);

        fetchResponse = await fetch('modules/regions/assets/fvtt-regions-template-Adventure Outline Template.json');
        fetchBlob = await fetchResponse.blob();
        template = JSON.parse(await fetchBlob.text());
        await app._importTemplateFile(template);

        if (reloadFoundry) { window.location.reload(); }
}

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            id: "regionsBackupManager",
            height: "auto",
            width: 550,
            submitOnChange: true,
            tabs: [{ navSelector: ".tabs", contentSelector: "#regionsManageBackupsContent" }],
            template: REGIONS_CONFIG.TEMPLATES.MANAGE_BACKUPS,
            title: game.i18n.localize("REGIONS.BACKUPS.Manage")
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsCreateCustomTag = Hooks.on("regionsCreateCustomTag", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteCustomTag = Hooks.on("regionsDeleteCustomTag", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsUpdateCustomTags = Hooks.on("regionsUpdateCustomTags", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsUpdateRegion = Hooks.on("regionsUpdateRegion", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteRegion = Hooks.on("regionsDeleteRegion", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsCreateRegion = Hooks.on("regionsCreateRegion", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsCreateTemplate = Hooks.on("regionsCreateTemplate", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsUpdateTemplate = Hooks.on("regionsUpdateTemplate", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteTemplate = Hooks.on("regionsDeleteTemplate", this._forceUpdateFromHook.bind(this));
        }
    }

    _onChangeTab(event, tabs, active) {
        super._onChangeTab(event, tabs, active);

        switch (active) {
            case "regions":
                this.updateWikiLink("https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Backups#region-backups");
                break;
            case "templates":
                this.updateWikiLink("https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Backups#template-backups");
                break;
            case "customTags":
                this.updateWikiLink("https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Backups#custom-tag-backups");
                break;
            default:
                this.updateWikiLink("https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Backups");

        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', "[data-action]", this._handleButtonClick.bind(this));
    }

    getData(options) {
        let localBackups = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups);
        let localRegions = RegionsData.getAllRegions();
        let localTemplates = RegionsData.getTemplates();
        let localCustomTags = RegionsData.getCustomTags();
        let categories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);

        for(let r of localRegions) {
            r.categoryName = categories.find(c => c.id === r.categoryID)?.name;
        }

        const data = {
            localBackups,
            localRegions,
            localTemplates,
            localCustomTags
        }

        return data;
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        let detailID = clickedElement.data().regionId ?? clickedElement.parents('[data-detail-id]')?.data()?.detailId;

        let needsRefresh = false;
        let fileImport = null;

        switch (action) {
            case "create-local":
                await RegionsData.createLocalBackup();
                needsRefresh = true;
                break;
            case "delete-local":
                let toDelete = RegionsData.getBackupByID(detailID);
                let toDeleteContent = `${game.i18n.localize("REGIONS.BACKUPS.ConfirmDeleteLocalText")}: ${toDelete.backupDate}`;
                let fDelete = async () => {
                    await RegionsData.deleteBackupByID(detailID);
                    this.render();
                }
                new Dialog({
                    title: game.i18n.localize("REGIONS.BACKUPS.DeleteLocal"),
                    content: toDeleteContent,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("REGIONS.BACKUPS.Delete"),
                            callback: (html) => {
                                fDelete();
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.BACKUPS.Cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case "download-custom-tag":
                await this._downloadBackup(detailID, RegionsBackupTypes.CUSTOMTAG);
                break;
            case "download-custom-tags-full":
                await this._downloadBackup(null, RegionsBackupTypes.CUSTOMTAG);
                break;
            case "download-full":
                await this._downloadBackup();
                break;
            case "download-local":
                await this._downloadBackup(detailID);
                break;
            case "download-region":
                await this._downloadBackup(detailID, RegionsBackupTypes.REGION);
                break;
            case "download-template":
                await this._downloadBackup(detailID, RegionsBackupTypes.TEMPLATE);
                break;
            case "perform-custom-tag-import":
                fileImport = clickedElement.parents("form").find("input[name='regionsImportCustomTagFile']")[0];
                if (!fileImport.files.length) { 
                    Logger.error(game.i18n.localize("REGIONS.ERRORS.import-no-file"));
                    return false;
                }

                readTextFromFile(fileImport.files[0]).then(async jsonText => {
                    let json = JSON.parse(jsonText);
                    Logger.debug("Import File:", json, jsonText);
                    if (!json.regionsVersion || json.backupType !== RegionsBackupTypes.CUSTOMTAG) {
                        Logger.error(game.i18n.localize("Invalid Import File"));
                        return false;
                    }

                    await this._importCustomTagFile(json);
                });
                break;
            case "perform-full-import":
                fileImport = clickedElement.parents("form").find("input[name='regionsImportGlobalFile']")[0];
                if (!fileImport.files.length) { 
                    Logger.error(game.i18n.localize("REGIONS.ERRORS.import-no-file"));
                    return false;
                }

                readTextFromFile(fileImport.files[0]).then(async jsonText => {
                    let json = JSON.parse(jsonText);
                    Logger.debug("Import File:", json, jsonText);
                    if (!json.regionsVersion || json.backupType !== RegionsBackupTypes.GLOBAL) {
                        Logger.error(game.i18n.localize("Invalid Import File"));
                        return false;
                    }

                    await this._importRegionsFile(json);
                });
                break;
            case "perform-migration":
                await Regions.migrateData(true);
                break;
            case "perform-region-import":
                fileImport = clickedElement.parents("form").find("input[name='regionsImportRegionFile']")[0];
                if (!fileImport.files.length) { 
                    Logger.error(game.i18n.localize("REGIONS.ERRORS.import-no-file"));
                    return false;
                }

                readTextFromFile(fileImport.files[0]).then(async jsonText => {
                    let json = JSON.parse(jsonText);
                    Logger.debug("Import File:", json, jsonText);
                    if (!json.regionsVersion || json.backupType !== RegionsBackupTypes.REGION) {
                        Logger.error(game.i18n.localize("Invalid Import File"));
                        return false;
                    }

                    await this._importRegionFile(json);
                });
                break;
            case "perform-reset":
                let performResetContent = `${game.i18n.localize("REGIONS.BACKUPS.ConfirmResetText")}`;

                new Dialog({
                    title: game.i18n.localize("REGIONS.BACKUPS.ConfirmReset"),
                    content: performResetContent,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("REGIONS.BACKUPS.PerformReset"),
                            callback: async (html) => {
                                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, {customTags: [], associations: [], templates: []});
                                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, []);
                                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, []);
                                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups, {});
                                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_scene_behavior, false);
                                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_player_view, false);
                                await RegionsData.updateFolders();
                        

                                await RegionsBackupManagerApplication.importSampleData(true);

                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.BACKUPS.Cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case "perform-template-import":
                fileImport = clickedElement.parents("form").find("input[name='regionsImportTemplateFile']")[0];
                if (!fileImport.files.length) { 
                    Logger.error(game.i18n.localize("REGIONS.ERRORS.import-no-file"));
                    return false;
                }

                readTextFromFile(fileImport.files[0]).then(async jsonText => {
                    let json = JSON.parse(jsonText);
                    Logger.debug("Import File:", json, jsonText);
                    if (!json.regionsVersion || json.backupType !== RegionsBackupTypes.TEMPLATE) {
                        Logger.error(game.i18n.localize("Invalid Import File"));
                        return false;
                    }

                    await this._importTemplateFile(json);
                });
                break;
            case "restore-local":
                let localRestoreBackup = RegionsData.getBackupByID(detailID);
                let localRestoreContent = `${game.i18n.localize("REGIONS.BACKUPS.ConfirmRestoreText")}: ${localRestoreBackup.backupDate}`;
                let fRestore = async() => {
                    await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version, localRestoreBackup.regionsVersion);
                    await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, localRestoreBackup.documents);
                    await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, localRestoreBackup.categories);
                    await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, localRestoreBackup.regions);

                    window.location.reload();
                }

                new Dialog({
                    title: game.i18n.localize("REGIONS.BACKUPS.ConfirmRestore"),
                    content: localRestoreContent,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("REGIONS.BACKUPS.RestoreLocal"),
                            callback: (html) => {
                                fRestore();
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.BACKUPS.Cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case "restore-templates":
                await RegionsBackupManagerApplication.importSampleData();
                needsRefresh = true;
                break;
            default:
                break;
        }

        if (needsRefresh) {
            this.render(true);
        }
    }

    async _forceUpdateFromHook() {
        this.render(true);
    }

    async _downloadBackup(backupID = null, backupType) {
        let dataExport = null;
        let dataName = null;

        switch (backupType) {
            case RegionsBackupTypes.CUSTOMTAG:
                if (backupID) {
                    dataExport = new RegionsCustomTagBackup(backupID);
                    dataName = `${dataExport.backupDate}-customTag-${dataExport.customTags[0].name}`;
                } else {
                    dataExport = new RegionsCustomTagBackup();
                    dataName = `${dataExport.backupDate}-customTag-complete`;
                }
                break;
            case RegionsBackupTypes.REGION:
                dataExport = await RegionsRegionBackup.CreateRegionBackupForRegionID(backupID);
                dataName = `${dataExport.backupDate}-region-${dataExport.region.name}`;
                break;
            case RegionsBackupTypes.TEMPLATE:
                dataExport = new RegionsTemplateBackup(backupID);
                dataName = `${dataExport.backupDate}-template-${dataExport.template.name}`;
                break;
            default:
                //Local global backup
                if (backupID) {
                    dataExport = RegionsData.getBackupByID(backupID);
                } else {
                    dataExport = new RegionsGlobalBackup();
                }

                dataName = `${dataExport.backupDate}-complete`;
                break;
        }

        if (!dataExport) { return; }

        let fileName = `fvtt-regions-${game.world.id}-${dataName}.json`;
        let exportedJSON = JSON.stringify(dataExport);
        saveDataToFile(exportedJSON, "text/json", fileName);
    }

    async _importRegionsFile(data, userAgreed = false) {
        if (!userAgreed && data.worldID != game.world.id) {
            let _this = this;
            new Dialog({
                title: game.i18n.localize("REGIONS.BACKUPS.PleaseConfirm"),
                content: game.i18n.localize("REGIONS.BACKUPS.DifferentWorld"),
                buttons: {
                    yes: {
                        icon: "<i class='fas fa-check'></i>",
                        label: game.i18n.localize("REGIONS.BACKUPS.RestoreLocal"),
                        callback: (html) => {
                            _this._importRegionsFile(data, true);
                        }
                    },
                    no: {
                        icon: "<i class='fas fa-times'></i>",
                        label: game.i18n.localize("REGIONS.BACKUPS.Cancel")
                    }
                },
                default: "no"
            }).render(true);

            return;
        }

        let preRestoreBackup = new RegionsGlobalBackup();
        try {
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version, data.regionsVersion);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, data.documents);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, data.categories);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, data.regions);

            await RegionsData.updateFolders();

            window.location.reload();
        } catch (e) {
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version, preRestoreBackup.regionsVersion);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, preRestoreBackup.documents);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, preRestoreBackup.categories);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, preRestoreBackup.regions);

            Logger.error(game.i18n.localize("REGIONS.ERRORS.ErrorAttemptingToRestore"), e. data);
        }
    }

    async _importCustomTagFile(data) {
        try {
            let documentData = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);

            for (let tag of data.customTags) {
                documentData.customTags = documentData.customTags.filter(c => c.id !== tag.id).concat(tag);
            }

            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documentData);
            Hooks.call("regionsUpdateCustomTags", data.customTags);
        } catch (e) {
            Logger.error(game.i18n.localize("REGIONS.ERRORS.ErrorAttemptingToImport"), e, data);
        }
    }

    async _importRegionFile(data, userAgreed = false) {
        if (!userAgreed && data.worldID != game.world.id) {
            let _this = this;
            new Dialog({
                title: game.i18n.localize("REGIONS.BACKUPS.PleaseConfirm"),
                content: game.i18n.localize("REGIONS.BACKUPS.DifferentWorld"),
                buttons: {
                    yes: {
                        icon: "<i class='fas fa-check'></i>",
                        label: game.i18n.localize("REGIONS.BACKUPS.RestoreLocal"),
                        callback: (html) => {
                            _this._importRegionsFile(data, true);
                        }
                    },
                    no: {
                        icon: "<i class='fas fa-times'></i>",
                        label: game.i18n.localize("REGIONS.BACKUPS.Cancel")
                    }
                },
                default: "no"
            }).render(true);

            return;
        }

        try {
            let regionCategories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
            let associationCategories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);
            let categories = RegionsData.getCategories();
            let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
            let regions = RegionsData.getAllRegions();

            let importRegionCategories = data.categories.filter(c => c.type === REGIONS_CONFIG.CATEGORY_TYPES.REGION);
            let importAssociationCategories = data.categories.filter(c => c.type === REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);

            for (let c of importRegionCategories) {
                let found = regionCategories.find(rc => rc.id === c.id);
                if (!found) {
                    categories = categories.concat(c);
                }
            }

            for (let c of importAssociationCategories) {
                let found = associationCategories.find(rc => rc.id === c.id);
                if (!found) {
                    categories = categories.concat(c);
                }
            }

            let existingRegion = regions.find(r => r.id === data.region.id);
            let associations = Array.from(data.region.associations);
            if (existingRegion) {
                await RegionsData.deleteRegion(existingRegion.id);
                regions = regions.filter(r => r.id != existingRegion.id);
                documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
            }

            documents.associations = documents.associations.concat(associations);
            delete data.region.associations;
            regions = regions.concat(data.region);

            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, categories);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documents);
            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);

            await RegionsData.updateFolders();
            Hooks.call("regionsCreateRegion", data.region);
            Hooks.call("regionsUpdateCategory", data.categories);
        } catch (e) {
            Logger.error(game.i18n.localize("REGIONS.ERRORS.ErrorAttemptingToImport"), e, data);
        }
    }

    async _importTemplateFile(data) {
        try {
            let regionCategories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
            let associationCategories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);
            let categories = RegionsData.getCategories();
            let templates = RegionsData.getTemplates();

            let importRegionCategories = data.categories.filter(c => c.type === REGIONS_CONFIG.CATEGORY_TYPES.REGION);
            let importAssociationCategories = data.categories.filter(c => c.type === REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);

            for (let c of importRegionCategories) {
                let found = regionCategories.find(rc => rc.id === c.id);
                if (!found) {
                    categories = categories.concat(c);
                }
            }

            for (let c of importAssociationCategories) {
                let found = associationCategories.find(rc => rc.id === c.id);
                if (!found) {
                    categories = categories.concat(c);
                }
            }

            let existingTemplate = templates.find(t => t.id === data.template.id);
            if (existingTemplate) {
                await RegionsData.deleteTemplate(existingTemplate.id);
                templates = templates.filter(t => t.id != existingTemplate.id);
            }

            await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, categories);
            await RegionsData.createTemplate(data.template);
            Hooks.call("regionsUpdateCategory", data.categories);
        } catch (e) {
            Logger.error(game.i18n.localize("REGIONS.ERRORS.ErrorAttemptingToImport"), e, data);
        }
    }

    async _updateObject(event, formData) {
    }
}