import { REGIONS_CONFIG } from "./helpers/config.mjs";
import { RegionsAPI } from "./helpers/api.mjs";
import { RegionsManageRegionsDialog } from "./forms/regions-manage-regions.mjs";
import { RegionInfo } from "./models/region-info.mjs";
import { RegionAssociation } from "./models/region-association.mjs";
import { ExtendedContextMenu } from "./helpers/extended-context-menu.mjs";
import { RegionsCustomTag } from "./models/region-custom.mjs";
import { RegionsTemplate } from "./models/region-template.mjs";
import { RegionsManageCustomDialog } from "./forms/regions-manage-custom.mjs";
import { RegionsManageTemplatesDialog } from "./forms/regions-manage-templates.mjs";
import { RegionsData } from "./helpers/data.mjs";
import { AboutDialog } from "./about/about-dialog.mjs";
import { Logger } from "./logger/logger.mjs";
import { RegionsImportCompediumDialog } from "./forms/regions-import-compendium.mjs";
import { HelpFormApplication } from "./about/help-form-application.mjs";
import { RegionsBackupManagerApplication } from "./forms/regions-backup-manager.mjs";

export class Regions {
    static initialize() {
        Logger.info("Initializing Regions");

        REGIONS_CONFIG.FOUNDRYVERSION = game.version ?? game.data.version;
        HelpFormApplication.localizeAbout = "REGIONS.about";
        HelpFormApplication.localizeHelp = "REGIONS.help";

        game.modules.get(REGIONS_CONFIG.ID).api = RegionsAPI;
        game.modules.get(REGIONS_CONFIG.ID).classes = {
            Region: RegionInfo,
            Association: RegionAssociation,
            Custom: RegionsCustomTag,
            Template: RegionsTemplate
        }

        this.regionsConfig = new RegionsManageRegionsDialog();

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version, {
            config: false,
            default: '',
            scope: 'world',
            type: String
        });

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, {
            config: false,
            default: [],
            scope: 'world',
            type: Array
        });

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, {
            config: false,
            default: {customTags: [], associations: [], templates: []},
            scope: 'world',
            type: Object
        });

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, {
            config: false,
            default: [],
            scope: 'world',
            type: Array
        });

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups, {
            config: false,
            default: {},
            scope: 'world',
            type: Object
        })

        game.settings.registerMenu(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_about, {
            name: game.i18n.localize("REGIONS.about"),
            label: game.i18n.localize("REGIONS.about"),
            restricted: false,
            icon: 'fas fa-info-circle',
            type: AboutDialog
        });

        game.settings.registerMenu(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups_menu, {
            name: game.i18n.localize("REGIONS.BACKUPS.ManageHint"),
            label: game.i18n.localize("REGIONS.BACKUPS.Manage"),
            restricted: true,
            icon: 'fas fa-file-export',
            type: RegionsBackupManagerApplication
        })

        game.settings.registerMenu(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_templates, {
            name: game.i18n.localize("REGIONS.TEMPLATE.Manage"),
            label: game.i18n.localize("REGIONS.TEMPLATE.Manage_Button"),
            restricted: true,
            icon: 'fas fa-clipboard-list',
            type: RegionsManageTemplatesDialog
        });

        game.settings.registerMenu(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_tags, {
            name: game.i18n.localize("REGIONS.CUSTOM_TAG.Manage"),
            label: game.i18n.localize("REGIONS.CUSTOM_TAG.Manage_Button"),
            restricted: true,
            icon: 'far fa-address-card',
            type: RegionsManageCustomDialog
        });

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_scene_behavior, {
            name: game.i18n.localize("REGIONS.CONFIG.Scene-Behavior.name"),
            hint: game.i18n.localize("REGIONS.CONFIG.Scene-Behavior.hint"),
            restricted: true,
            config: true,
            default: false,
            scope: 'world',
            type: Boolean
        });

        // Depracated, here to enable conversion only
        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_custom_keys, {
            name: game.i18n.localize("REGIONS.CONFIG.Custom-Keys.name"),
            hint: game.i18n.localize("REGIONS.CONFIG.Custom-Keys.hint"),
            restricted: true,
            config: false,
            default: '',
            scope: 'world',
            type: String
        });

        game.settings.register(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_player_view, {
            name: game.i18n.localize("REGIONS.CONFIG.Enable-Player-View.name"),
            hint: game.i18n.localize("REGIONS.CONFIG.Enable-Player-View.hint"),
            restricted: true,
            config: true,
            default: false,
            scope: 'world',
            type: Boolean
        });

        return this.preloadHandlebarTemplates();
    }

    static preloadHandlebarTemplates = async function() {
        //Partials
        return loadTemplates([
            "modules/regions/templates/partials/documents.hbs",
            "modules/regions/templates/partials/custom-keys.hbs"
        ]);
    }

    static renderNavButton(html, contextOptions) {
        //Find the expand/collapse button, we'll be appending right after that.
        if (game.user.isGM || this.allowPlayerView()) {
            const navToggleButton = html.find('#nav-toggle');
            const toolTip = game.user.isGM ? game.i18n.localize('REGIONS.regions-button') : game.i18n.localize('REGIONS.regions-overview');
            navToggleButton.before(
                `<button id='foundryRegionsEditor' type="button" class="regionsNavigationButton nav-item flex0" title="${toolTip}"><i class="fas fa-globe"></i></button>`
            );

            html.on('click', '#foundryRegionsEditor', (event) => {
                Regions.regionsConfig.render(true);
            });

            new ExtendedContextMenu(html, '#foundryRegionsEditor', this.contextMenu());
        }
    }

    static renderSidebarDirectory(html, options) {
        options.push(
            {
                name: "REGIONS.import-to-regions",
                icon: "<i class='fas fa-file-import'></i>",
                callback: (selection) => {
                    let regions = RegionsData.getAllRegions();
                    if (!regions.length) {
                        Logger.warn(game.i18n.localize("REGIONS.WARNINGS.NoRegionsExist"));
                        return;
                    }
                    const pack = selection.data().pack;
                    const dialog = new RegionsImportCompediumDialog({compendiumKey: pack});
                    dialog.render(true);
                }
            }
        );
    }

    static get version() {
        let version = isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10") ? game.modules.get("regions").version : game.modules.get("regions").data.version;
        return version;
    }

    static allowPlayerView() {
        return game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_player_view);
    }

    static async ready() {
        await this.migrateData();
        await RegionsData.updateFolders();
    }

    static async migrateData(force = false) {
        let currentVersion = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version);
        let newVersion = Regions.version;

        if (isNewerVersion(newVersion, currentVersion) || force) {
            //User may have multiple migrations that are needed, so we'll step through each.
            let migrateVersions = ["1.1.0", "1.2.0", "1.3.0", "2.0.0"];
            
            if (!force) {
                migrateVersions = migrateVersions[migrateVersions.length - 1];
            } else {
                migrateVersions.filter(v => v > currentVersion);
            }
            Logger.info(`${game.i18n.localize("REGIONS.migrating")} ${newVersion}`);
            let regions = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data);

            for (let migrateVersion of migrateVersions) {
                switch (migrateVersion) {
                    case '2.0.0':
                        //Let's create a quick local backup
                        await RegionsData.createLocalBackup();
                        Logger.info(game.i18n.localize("REGIONS.migration-backup-created"));

                        //Merge associations, convert custom keys to custom tags
                        let data = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
                        for (let r of regions) {
                            for (let a of r.associations) {
                                if (a.compendiumKey) {
                                    a.compendiumID = a.foundryID;
                                }
                            }
                            
                            let customKeys = r.associations.filter(a => a.type === REGIONS_CONFIG.TYPES.CUSTOM);
                            r.categoryID = null;
                            r.typeID = null;

                            for (let ck of customKeys) {
                                let customTag = new RegionsCustomTag();
                                customTag.value = ck.value;
                                customTag.name = ck.key;
                                customTag.id = ck.id;

                                ck.customData = customTag;
                            }

                            data.associations.push.apply(data.associations, r.associations);
                            delete r["associations"];
                        }

                        game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);
                        game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, data);

                        RegionsBackupManagerApplication.importSampleData(false);
                        break;
                    case '1.3.0':
                        //With the addition of player view, default all regions to private
                        for (let r of regions) {
                            r.private = true;
                        }

                        game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);

                        break;
                    case '1.2.0':
                        //Add regionID to all associations
                        for (let r of regions) {
                            for (let a of r.associations) {
                                a.regionID = r.id;
                            }
                        }
    
                        game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);

                        await RegionsAPI.updateFolders();
                        break;
                    case '1.1.0':
                        //All scene associations need to have their image updated
                        for (let r of regions) {
                            for(let a of r.associations) {
                                if (a.type === this.TYPES.SCENE) {
                                    let scene = null;
                                    if (a.compendiumKey) {
                                        let pack = game.packs.get(a.compendiumKey);
                                        scene = await pack.getDocument(a.foundryID);
                                    } else {
                                        scene = game.scenes.get(a.foundryID);
                                    }
        
                                    a.foundryIMG = isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10") ? scene.thumb : scene.data.thumb;
                                }
                            }
                        }
    
                        game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);
                        break;
                    default:
                        break;
                }
            }
    
            Logger.info(`${game.i18n.localize("REGIONS.migration-complete")}`);
            game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version, newVersion);
        }
    }

    static contextMenu() {
        let regions = RegionsAPI.getAllRegions();
        if (!game.user.isGM) {
            regions = regions.filter(r => !r.private);
        }

        let contextMenu = [];

        for (let r of regions) {
            (async () => { return await RegionsAPI.getRegion(r.id); })().then(result => {
                contextMenu.push(result.buildRegionContextMenu());
            });
        }

        if (regions.length < 1) {
            contextMenu.push({
                icon: "",
                classes: ["regionsNoRegionDefined"],
                name: game.i18n.localize("REGIONS.no-regions-defined"),
                callback: async (c) => {
                    return;
                }
            })
        }

        return contextMenu;
    }

    /* Prepare a once hook if required */
    static handleCanvasDrop(c, data) {
        if (data?.from === "Regions" && data.association.compendiumKey) {
            let association = data.association;

            switch (data.association.type) {
                case Regions.TYPES.ACTOR:
                    Hooks.once('preCreateActor', (doc, createData, options, userId) => { RegionsAPI.preCreate(doc, createData, options, userId, association); });
                    break;
                case Regions.TYPES.JOURNAL:
                    if (isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "0.8.8")) {
                        //v0.8.9 introduced importing journals from compendiums; so we can handle this similar to actors.
                        Hooks.once('preCreateJournalEntry', (doc, createData, options, userId) => { RegionsAPI.preCreate(doc, createData, options, userId, association); });
                    } else if (data.pack) {
                        //handleCanvasDrop isn't async!
                        //We need to return false to deny the drop event
                        Logger.warn(game.i18n.localize("REGIONS.journal-imported"));
                        RegionsAPI.importJournal(data.association);
                        return false;
                    }
                    break;
                default:
                    break;
            }
        }

        return true;
    }
}