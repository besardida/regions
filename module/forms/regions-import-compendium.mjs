import { Logger } from "../logger/logger.mjs";
import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";
import { RegionAssociation } from "../models/region-association.mjs";
import { RegionInfo } from "../models/region-info.mjs";

export class RegionsImportCompediumDialog extends HelpFormApplication {
    #compendiumKey = null;
    #compendiumName = null;
    #regionID = null;
    #hookIndexes = {
        "regionsCreateRegion": null,
        "regionsDeleteRegion": null,
        "regionsUpdateRegion": null,
        "closeApplication": null
    };

    constructor(object, options) {
        if (!object) { object = { }; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Edit-Region#import-compendium-to-regions";
        super(object, options);

        this.#compendiumKey = object.compendiumKey;
        this.#compendiumName = game.packs.get(this.#compendiumKey).metadata.label;
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsCreateRegion = Hooks.on("regionsCreateRegion", this._forceUpdateFromHook);
            this.#hookIndexes.regionsDeleteRegion = Hooks.on("regionsDeleteRegion", this._forceUpdateFromHook);
            this.#hookIndexes.regionsUpdateRegion = Hooks.on("regionsUpdateRegion", this._forceUpdateFromHook);
        }
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: 'auto',
            width: '500',
            id: 'regionsImportCompendiumDialog',
            classes: ['regions'],
            submitOnChange: true,
            template: REGIONS_CONFIG.TEMPLATES.IMPORT_COMPENDIUM,
        }

        const mergedOptions = foundry.utils.mergeObject(defaults, overrides);

        return mergedOptions;
    }

    get title() {
        return `${game.i18n.localize("REGIONS.import-compendium")}: ${this.#compendiumName}`;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', "[data-action]", this._handleButtonClick.bind(this));
    }

    getData(options) {
        let regions = RegionsData.getAllRegions().map((r) => { return { id: r.id, name: r.name !=  null ? r.name : game.i18n.localize("REGIONS.new-region")}});
        let pack = game.packs.get(this.#compendiumKey);
        let type = pack.documentName;
        let count = pack.index.size;
        let preview = Array.from(pack.index.contents).map((c) => {
            return {
                _id: c._id,
                name: c.name,
                img: pack.documentName === REGIONS_CONFIG.TYPES.PLAYLIST ? "icons/svg/sound.svg" : (pack.documentName === REGIONS_CONFIG.TYPES.JOURNAL && !c.img) ? "icons/svg/book.svg" : c.img
            }
        });

        const data = {
            regions,
            type,
            count,
            preview,
            regionID: this.#regionID
        }

        Logger.debug(data);

        return data;
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;

        switch (action) {
            case "cancel":
                this.close();
                break;
            case "submit":
                if (!this.#regionID) { return; }

                let pack = game.packs.get(this.#compendiumKey);
                let contents = Array.from(pack.index.contents);
                const associations = contents.map((c) => { 
                    return new RegionAssociation({
                        compendiumKey: this.#compendiumKey,
                        compendiumID: c._id, 
                        foundryID: c._id,
                        foundryIMG: pack.documentName === REGIONS_CONFIG.TYPES.PLAYLIST ? "icons/svg/sound.svg" : (pack.documentName === REGIONS_CONFIG.TYPES.JOURNAL && !c.img) ? "icons/svg/book.svg" : c.img,
                        foundryName: c.name, 
                        regionID: this.#regionID, 
                        type: pack.documentName
                    })
                });

                const region = await RegionInfo.init(this.#regionID);
                region.associations = region.associations.concat(associations);
                await RegionsData.updateRegion(region);

                this.close({force: true});
                break;
            default:
                break;
        }
    }

    async _updateObject(event, formData) {
        this.#regionID = formData.regionID;
    }

    _forceUpdateFromHook = (region) => {
        this.render(true);
    }
}