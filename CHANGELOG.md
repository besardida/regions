# Changelog
## v2.0.0 - 2022-07-22
 - Regions now save automatically, without needing to click a save button!
 - Can now associate a Region with another Region!
 - Custom Keys has been completely replaced and overhauled with new Custom Tags!
    - Custom Tags currently support the following types:
        - Text
        - Numeric
        - Drop Down List
        - Counter
        - Checkbox
        - Slider
    - Existing custom keys will automatically be converted to Custom Tags (Text).
 - Categories!  You can now create categories for your Regions and their associations, helping to better organize them!
 - Templates!  You can now create custom templates that you can apply to your Regions, automatically populating them with organized Custom Tags.
 - New feature to add an entire Compendium to a Region!  Simply right click on the compendium and choose Import to Region
 - Ability to drag & drop an entire folder of documents into a Region!
 - Updated API for interacting with Regions via macros, world scripts, or other modules.
 - Full overhaul of much of the styling and presentation of the module
 - New added help menus to each screen of Regions
 - Support for Foundry v10!

## v1.3.1 - 2021-11-20
### Bug Fixes
- Corrected a compatibility issue with minimual UI

### Features
- Extended the context menu to allow for quick navigation to any information associated with a region.  Scenes will automatically view/activate (based on module settings) and playlists will automatically play.

### Known Bugs
- The context menu will not update submenu items until after a scene change.

## v1.3.0 - 2021-10-17
### Bug Fixes
- Corrected a bug where duplicate Regions subfolders would keep getting created.

### Features
- Created a new "Player View" (disabled by default) that allows non-GM accounts to view some Region information.
- Created an Import/Export function to allow individual Regions (or all regions) to be exported, and then subsequently imported into other worlds.  Because of the way Region data is stored, it requires that the same internal ID's be used between worlds.
- Added an internal Description field to all associations, viewable by GMs only, allowing you to add notes to every association you create.
- Overhauled and updated the backend system due to code creep.

## v1.2.1 - 2021-08-18
### Bug Fixes
- Corrected format issue in module.json

## v1.2.0 - 2021-08-18
### Features
- Added ability to drag & drop actors and journals directly from a region onto the canvas
- Added import folders per region.  Scenes, Actors, and Journal Entries now have a "Regions" folder, within which each region gets its own folder.  All imports that occur through Regions will be placed in the appropriate folder.
- Split "Save" button into "Save & Close" and "Save Changes", to provide a method for saving all changes without closing the editor.
- Added a "Full List" view, that displays all associations within a region and can expand/collapse each association type via its header.

### Bug Fixes
- Fixed bug where region notes were not automatically saving when clicking the "save" button.

### Known Bugs
- Adding and adjusting custom keys are not reflected properly between the "Full List" and "Custom Tags" tabs.  Recommended workaround is to modify in just one of those areas (swapping tabs will not lose changes).

## v1.1.1 - 2021-08-08
### Bug Fixes
- Resolved issue with a custom tag automatically being injected into regions
- Fixed bug with the regions icon automatically expanding to match the size of the navigation bar.
- Fixed bug that might prevent a region from opening if there was an issue with the custom keys.

## v1.1.0 - 2021-08-08
### Features
- Added ability to drag actors onto canvas to create tokens
- Added migration code for newer versions
- Added custom keys

### Bug Fixes
- Resolved issue where scenes were loading the full background image, causing a slowdown

## v1.0.1 - 2021-08-06
### Features
- Added new configuration option for activating/viewing scenes when clicking on their icons.
  
### Bug Fixes
- Resolved issue with the title of new regions being displayed as null when editing them.

## v1.0.0 - 2021-08-06
- First release of Regions