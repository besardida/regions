# Regions
Regions is a world-building/adventure writing assistant that enables you to link together all of your Foundry documents - Scenes, Actors, Items, Journals, Rolltables and Playlists - combined with a number of custom tags that together help you to organize central concepts of your world.

## Usage 
Almost all of Regions is based on simple drag & drop - be it a single Actor or an entire folder of them.

Quick shortcuts via right clicking on the the Regions Icon can let you swiftly swap to scenes, open actors or rolltables, or even immediately start playing a playlist that is associated with a Region.

For more detailed breakdown of using Regions, visit the Regions Wiki at https://gitlab.com/geekswordsman/regions/-/wikis/home

## Support Me!
This module is free for use, however if you enjoy it and wish to help support the developer, please visit my Ko-Fi and leave a tip! [Geekswordsman's Ko-Fi!](https://ko-fi.com/geekswordsman)

## Attribution
Provided templates based off [The Chronodex](http://TheChronodex.com) by Sarah Kaiser (@KaiserMakes), licensed under 2018 CC BY-NC 4.0