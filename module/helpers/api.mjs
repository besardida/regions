import { Regions } from '../regions.mjs';
import { REGIONS_CONFIG } from './config.mjs';
import { RegionsData } from './data.mjs';

export class RegionsAPI extends RegionsData {
    /**
     * _responseCode - standardized response object
     * @param data (object) - the data package returned using by this response
     * @param code (int) - enumerated response code, default OK (0)
     * @param msg (string) - user friendly message to help with code - default empty (OK)
     * @param error (boolean) - quick access boolean to determine if an error was encountered in this request, default false.
     * 
     * @returns object comprising of the parameters passed in.
     */
    static _responseCode(data = {}, code = REGIONS_CONFIG.CODES.OK, msg = '', error = false) {
        return {
            code, msg, data, error
        }
    }

    /**
     * getDocumentAssociationInformation - Searches region information for a given FoundryID and returns back any associated region information.
     * @param foundryID - Foundry's internal ID for the associated document.
     * 
     * @returns An array of regional information.
     */
    static getDocumentAssociationInformation(foundryID) {
        let response = this._responseCode({foundryID, regions: []});

        try {
            let regions = RegionsData.getAllRegions().filter(r => r.associations.filter(a => a.foundryID === foundryID).length > 0);
            if (regions.length) {
                for (let region of regions) {
                    response.data.regions.push({
                        id: region.id,
                        name: region.name,
                        customKeys: region.associations.filter(a => a.type === REGIONS_CONFIG.TYPES.CUSTOM)
                    });
                }
            } else {
                response.code = REGIONS_CONFIG.CODES.NOT_FOUND;
            }
        } catch (e) {
            response.error = true;
            response.msg = `Regions encountered an error while processing a getDocumentAssociationInformation request: ${e.message}`;
            response.code = REGIONS_CONFIG.CODES.INTERNAL_ERROR;
        }

        return response;
    }

    /**
     * getRegionsBasedOnCustomKeyValue - Searches region information for a given custom key and its value and retuns back any associated region information.
     * @param {*} customKey - The custom key to search for
     * @param {*} customValue - The value the key should have; this is automatically compared in lower case.
     * 
     * @returns An array of regional information
     */
    static getRegionsBasedOnCustomKeyValue(customKey, customValue) {
        let response = this._responseCode({customKey, customValue, regions: []});

        try {
            let regions = RegionsData.getAllRegions().filter(r => r.associations.filter(a => a.type === 'Custom' && a. key === customKey && a.value.toLowerCase() === customValue.toLowerCase()).length > 0);
            if (regions.length) {
                for (let region of regions) {
                    response.data.regions.push({
                        id: region.id,
                        name: region.name,
                        customKeys: region.associations.filter(a => a.type === REGIONS_CONFIG.TYPES.CUSTOM)
                    });
                }
            } else {
                response.code = REGIONS_CONFIG.CODES.NOT_FOUND;
            }
        } catch (e) {
            response.error = true;
            response.msg = `Regions encountered an error while processing a getRegionsBasedOnCustomKeyValue request: ${e.message}`;
            response.code = REGIONS_CONFIG.CODES.INTERNAL_ERROR;
        }

        return response;
    }

    /* Hooks */
    /* These are internal methods, called by Regions to fire hooks when necessary. */

    /**
     * onRegionsSceneActivate - Fires a hook when a scene within a region is activated.
     * @param {*} scene - The scene data for the newly activated scene
     * @param {*} activeScene - Quick scene information for the new activeScene.
     * @param {*} diffData - Is this a different scene, and are we rendering it
     * @param {*} messageID - the internal ID for the triggering hook
     * 
     * @returns void
     */
    static _onRegionsSceneActivate(scene, activeScene, diffData, messageID) {
        if (!activeScene?.active) { return; }
        let regionCheck = this.getDocumentAssociationInformation(activeScene._id);
        if (regionCheck.code != REGIONS_CONFIG.CODES.OK || !regionCheck.data?.regions?.length) { return; }

        for (let region of regionCheck.data.regions) {
            Hooks.call("onRegionsSceneActivate", region, activeScene._id);
        }
    }
}