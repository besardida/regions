import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsCustomTag } from "../models/region-custom.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";
import { Logger } from "../logger/logger.mjs";

export class RegionsEditCustomDialog extends HelpFormApplication {
    onClose = null;
    
    constructor(object, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Edit-Custom-Tag";
        super(object, options);

        if (!object) {
            object = {customData: {}};
        }

        this.customResource = object.customData ?? object;
        this.resourceTypes = REGIONS_CONFIG.KEY_TYPES;
        this.addedNewResource = false;
        this.customResourceNewName = "";
        this.disabled = true;

        if (!this.customResource.id) {
            //New resource
            this.disabled = false;
            this.customResource = new RegionsCustomTag();
            this.customResource.regionID = object.regionID;
        }

        if(this.customResource.type === 'SLIDER' && this.customResource.options.length !== 2) {
            this.customResource.options = [
                {
                    value: game.i18n.localize("REGIONS.CUSTOM_TAG.LeftValue"),
                    selected: false,
                    id: foundry.utils.randomID()
                },
                {
                    value: game.i18n.localize("REGIONS.CUSTOM_TAG.RightValue"),
                    selected: false,
                    id: foundry.utils.randomID()
                }
            ]
        }
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: "auto",
            width: 350,
            submitOnChange: true,
            template: REGIONS_CONFIG.TEMPLATES.EDIT_CUSTOM,
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }


    get title() {
        if (this.disabled) {
            return game.i18n.localize("REGIONS.CUSTOM_TAG.Edit");
        }

        return game.i18n.localize("REGIONS.CUSTOM_TAG.Create");
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
    }

    getData(options) {
        return {
            resourceName: this.customResource.name,
            resourceLimit: this.customResource.max,
            resourceType: this.customResource.type,
            resourceOptions: this.customResource.options,
            resourceSteps: this.customResource.steps,
            defaultValue: this.customResource.value,
            showResourceLimit: (this.customResource.type === "COUNTER" || this.customResource.type === "NUMERIC" || this.customResource.type === "SLIDER"),
            resourceTypes: this.resourceTypes,
            customResourceNewName: this.customResourceNewName,
            showListOptions: (this.customResource.type === "LIST" || this.customResource.type === "SLIDER"),
            disabled: this.disabled
        }
    }

    async _updateObject(event, formData) {
        if (formData["custom-resource-name"]) { this.customResource.name = formData["custom-resource-name"]; }
        if (formData["custom-resource-type"]) { this.customResource.type = formData["custom-resource-type"]; }
        if (formData["custom-resource-limit"] !== undefined) { this.customResource.max = formData["custom-resource-limit"]; }
        if (formData["custom-resource-new-option"]) { this.customResourceNewName = formData["custom-resource-new-option"]; }
        if (formData["custom-default-value"] || formData["custom-default-value"] === "") { this.customResource.value = formData["custom-default-value"]; }

        for (let option of this.customResource.options) {
            if (formData[`custom-resource-option-${option.id}`]) {
                option.value = formData[`custom-resource-option-${option.id}`];
            }
        }

        if (this.addedNewResource) {
            this.customResourceNewName = "";
            this.addedNewResource = false;
        }

        if (this.customResource.type === "COUNTER" && this.customResource.max === 0) {
            this.customResource.max = 5;
        }

        this.showResourceLimit = (this.customResource.type === "COUNTER" || this.customResource.type === "NUMERIC");

        if (this.customResource.type === "SLIDER") {
            if (this.customResource.options.length != 2) {
                this.customResource.options = [
                    {
                        value: game.i18n.localize("REGIONS.CUSTOM_TAG.LeftValue"),
                        selected: false,
                        id: foundry.utils.randomID()
                    },
                    {
                        value: game.i18n.localize("REGIONS.CUSTOM_TAG.RightValue"),
                        selected: false,
                        id: foundry.utils.randomID()
                    }
                ]
            }
        } else if (this.customResource.type === "COUNTER") {
            if (this.customResource.steps.length < this.customResource.max) {
                this.customResource.steps = [];
                for (let i = 0; i < this.customResource.max; i++) { this.customResource.steps.push(false); }
            } else if (this.customResource.steps.length > this.customResource.max) {
                this.customResource.steps.splice(this.customResource.max, (this.customResource.steps.length - this.customResource.max));
            }
        }

        await this.validateSubmission();

        this.render(false);
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;

        switch (action) {
            case "custom-counter-adjust":
                let stepIndex = clickedElement.data().step;
                let newValue = !this.customResource.steps[stepIndex];
                if (stepIndex < this.customResource.max) {
                    if (this.customResource.steps[stepIndex + 1] === true) {
                        newValue = true;
                    }
                }
                for (let i = 0; i < stepIndex; i++) { this.customResource.steps[i] = true; }
                this.customResource.steps[stepIndex] = newValue;
                this.customResource.steps = this.customResource.steps.map((val, index) => { return (index > stepIndex ? false : val)});
                this.render(true);
                break;
            case "new-option":
                if (this.customResourceNewName.trim() === "") { this.customResourceNewName = game.i18n.localize("REGIONS.CUSTOM_TAG.New_Option"); }
                let newOption = {
                    value: this.customResourceNewName,
                    selected: false,
                    id: foundry.utils.randomID()
                }
                this.customResource.options.push(newOption);
                this.addedNewResource = true;
                break;
            case "delete-option":
                let deleteID = clickedElement.data().id;
                this.customResource.options = this.customResources.options.filter(r => r.id !== deleteID);
                break;
            case "cancel":
                this.close();
                break;
            case "submit":
                if (await this.validateSubmission()) {
                    if (!this.disabled) {
                        //Store new custom to main data
                        await RegionsData.createCustom(this.customResource);
                    }
        
                    if (this.onClose) { this.onClose(this.customResource); }
                    this.close();
                }
                break;
            default:
                break;
        }

        return true;
    }

    async validateSubmission() {
        let validName = false;
        let validLimit = true;

        this.customResource.name = this.customResource.name.trim();

        validName = this.customResource.name.length > 0;

        if (!validName) {
            Logger.warn(game.i18n.localize("REGIONS.WARNINGS.Invalid-Name"));
            return false;
        }

        if (this.customResource.type === "COUNTER" || this.customResource.type === "NUMERIC") {
            if (this.customResource.max < 0) { validLimit = false; }
            if (this.customResource.type == "COUNTER" && this.customResource.max < 2) { validLimit = false; }
        }

        if (!validLimit) {
            Logger.warn(game.i18n.localize("REGIONS.WARNINGS.Invalid-Limit"));
            return false;
        }

        let steps = null;

        if (!this.disabled) {
            switch (this.customResource.type) {
                case "COUNTER":
                    this.customResource.value = this.customResource.value ?? 0;
                    this.customResource.steps = [];
                    for (let i = 0; i < this.customResource.max; i++) { this.customResource.steps.push(false); }
                    break;
                case "TEXT":
                    this.customResource.value = this.customResource.value ?? "";
                    break;
                case "TOGGLE":
                case "CONDITION":
                    this.customResource.value = this.customResource.value ?? false;
                    break;
                default:
                    this.customResource.value = this.customResource.value ?? 0;
                    break;
            }
        } else {
            if (this.customResource.type === "COUNTER") {
                let currentSteps = this.customResource.steps;
                if (this.customResource.max != currentSteps.length) {
                    steps = [];
                    for (let i = 0; i < this.customResource.max; i++) {
                        steps.push(i < currentSteps.length ? currentSteps[i] : false);
                    }
                    this.customResource.steps = steps;
                }
            }
        }

        return true;
    }
}