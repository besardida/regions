import { REGIONS_CONFIG } from "./config.mjs";
import { RegionInfo } from "../models/region-info.mjs";
import { RegionsCategory } from "../models/regions-category.mjs";
import { Logger } from "../logger/logger.mjs";
import { RegionsGlobalBackup } from "../models/regions-backup.mjs";

export class RegionsData {
    /* Handle creation if needed */
    static preCreate(doc, createData, options, userId, association) {
        let region = this.getRegion(association.regionID);
        let folderID = null;
        switch (association.type) {
            case REGIONS_CONFIG.TYPES.ACTOR:
                folderID = region.folders.actorFolder;
                break;
            case REGIONS_CONFIG.TYPES.JOURNAL:
                folderID = region.folders.journalFolder;
                break;
            default:
                break;
        }

        if (isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10")) {
            doc.update({ folder: folderID });
        } else {
            doc.data.update({ folder: folderID });
        }
    }

    /* Check for and update Regions folders */
    static async updateFolders() {
        if (!game.user.isGM) { return; }
        let mainSceneFolder = game.folders.find(f => f.name === "Regions" && f.type === "Scene");
        let mainActorFolder = game.folders.find(f => f.name === "Regions" && f.type === "Actor");
        let mainJournalFolder = game.folders.find(f => f.name === "Regions" && f.type === "JournalEntry");
        let folderID = null;

        if (!mainSceneFolder) {
            folderID = randomID();
            mainSceneFolder = await Folder.implementation.create({ _id: folderID, name: "Regions", type: "Scene" }, { keepId: true });
        }
        if (!mainActorFolder) {
            folderID = randomID();
            mainActorFolder = await Folder.implementation.create({ _id: folderID, name: "Regions", type: "Actor" }, { keepId: true });
        }
        if (!mainJournalFolder) {
            folderID = randomID();
            mainJournalFolder = await Folder.implementation.create({ _id: folderID, name: "Regions", type: "JournalEntry" }, { keepId: true });
        }

        let foldersToCreate = [];
        let foldersToUpdate = [];
        let foldersToDelete = [];
        if (isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10")) {
            foldersToDelete = game.folders.filter(f => f.folder === mainSceneFolder.id).concat(game.folders.filter(f => f.folder === mainActorFolder.id)).concat(game.folders.filter(f => f.folder === mainJournalFolder.id));
        } else {
            foldersToDelete = game.folders.filter(f => f.data.parent === mainSceneFolder.id).concat(game.folders.filter(f => f.data.parent === mainActorFolder.id)).concat(game.folders.filter(f => f.data.parent === mainJournalFolder.id));
        }

        let regions = this.getAllRegions();

        for (let region of regions) {
            if (!region.hasOwnProperty("folders")) {
                region.folders = {
                    sceneFolder: null,
                    actorFolder: null,
                    journalFolder: null
                };
            }

            let sceneFolder = game.folders.get(region.folders.sceneFolder);
            let actorFolder = game.folders.get(region.folders.actorFolder);
            let journalFolder = game.folders.get(region.folders.journalFolder);

            let sceneFolderName = this.getImportFolderName(region, "Scene");
            let actorFolderName = this.getImportFolderName(region, "Actor");
            let journalFolderName = this.getImportFolderName(region, "JournalEntry");

            if (!sceneFolder) {
                sceneFolder = game.folders.getName(sceneFolderName);
                if (sceneFolder) {
                    region.folders.sceneFolder = sceneFolder.id;
                } else {
                    folderID = randomID();
                    foldersToCreate.push({ _id: folderID, name: sceneFolderName, parent: mainSceneFolder.id, type: "Scene" });
                    region.folders.sceneFolder = folderID;
                }
            } else {
                if (sceneFolder.name !== sceneFolderName) {
                    foldersToUpdate.push({ _id: sceneFolder.id, name: sceneFolderName });
                }
            }

            if (!actorFolder) {
                actorFolder = game.folders.getName(actorFolderName);
                if (actorFolder) {
                    region.folders.actorFolder = actorFolder.id;
                } else {
                    folderID = randomID();
                    foldersToCreate.push({ _id: folderID, name: actorFolderName, parent: mainActorFolder.id, type: "Actor" });
                    region.folders.actorFolder = folderID;
                }
            } else {
                if (actorFolder.name !== actorFolderName) {
                    foldersToUpdate.push({ _id: actorFolder.id, name: actorFolderName });
                }
            }

            if (!journalFolder) {
                journalFolder = game.folders.getName(journalFolderName);
                if (journalFolder) {
                    region.folders.journalFolder = journalFolder.id;
                } else {
                    folderID = randomID();
                    foldersToCreate.push({ _id: folderID, name: journalFolderName, parent: mainJournalFolder.id, type: "JournalEntry" });
                    region.folders.journalFolder = folderID;
                }
            } else {
                if (journalFolder.name !== journalFolderName) {
                    foldersToUpdate.push({ _id: journalFolder.id, name: journalFolderName });
                }
            }

            delete region.associations;
        }

        let folderChecks = regions.map(r => {return r.folders.sceneFolder; }).concat(regions.map(r => { return r.folders.actorFolder; })).concat(regions.map(r => { return r.folders.journalFolder; }));
        foldersToDelete = foldersToDelete.filter(f => !folderChecks.includes(f.id)).map(f => { return f.id});

        if (foldersToCreate.length > 0) {
            await Folder.createDocuments(foldersToCreate, { keepId: true });
        }

        if (foldersToUpdate.length > 0) {
            await Folder.updateDocuments(foldersToUpdate);
        }

        if (foldersToDelete.length > 0) {
            await Folder.deleteDocuments(foldersToDelete);
        }

        game.scenes.render(true);
        game.actors.render(true);
        game.journal.render(true);

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);

        Logger.debug("Updated folders for regions: ", regions);
    }

    /* Handle journal import when dropped onto a canvas */
    static async importJournal(association) {
        let region = this.getRegion(association.regionID);
        let pack = game.packs.get(association.compendiumKey);

        let journal = await game.journal.importFromCompendium(pack, association.foundryID, { folder: region.folders.journalFolder });
        return {
            id: journal.id,
            pack: null,
            type: "JournalEntry",
            x: association.x,
            y: association.y
        }
    }

    static async importRegionData(region) {
        if (!game.user.isGM) { return; }

        const oldRegions = this.getAllRegions();
        let idCheck = oldRegions.find(o => o.id === region.id);
        if (idCheck) {
            Logger.error(`Attempting to import region with ID ${region.id} failed because a region with that ID already exists.`);
            return;
        }
        const newRegions = [...oldRegions, region];

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, newRegions);
    }
    
    /* Create Category */
    static async createCategory(categoryType) {
        if (!game.user.isGM) { return; }

        let newCategory = new RegionsCategory(categoryType);
        const categories = [...this.getCategories(), newCategory];

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, categories);

        Hooks.call("regionsCreateCategory", newCategory);

        return newCategory;
    }

    /* Create a Region */
    static async createRegion() {
        if (!game.user.isGM) { return; }
        
        const newRegion = new RegionInfo();
        const oldRegions = this.getAllRegions();
        const newRegions = [...oldRegions, newRegion];

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, newRegions);
        await RegionsData.updateFolders();

        Hooks.call("regionsCreateRegion", newRegion);

        return newRegion;
    }

    /* Create a Custom Tag */
    static async createCustom(customTag) {
        if (!game.user.isGM) { return; }

        let documentData = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        documentData.customTags.push(customTag);
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documentData);

        Hooks.call("regionsCreateCustomTag", customTag);
    }

    /* Create a Template */
    static async createTemplate(template) {
        if (!game.user.isGM) { return; }

        let documentData = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        documentData.templates.push(template);

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documentData);
        
        Hooks.call("regionsCreateTemplate", template);
    }

    /**
     * createLocalBackup - Requests that a local backup be taken
     * 
     * @returns Promise, id of created backup
    */
    static async createLocalBackup() {
        if (!game.user.isGM) { return; }
        let backup = new RegionsGlobalBackup();

        let backups = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups);
        backups[backup.id] = backup;

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups, backups);

        return backup.id;
    }

    /**
     * getBackupByID - Requests a global backup by the given ID
     * @param {*} backupID - Internal ID of the backup to retrieve
     * 
     * @returns RegionsGlobalBackup object of the given ID if found
     */
    static getBackupByID(backupID) {
        if (!game.user.isGM) { return; }
        return game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups)[backupID];
    }
    
    /* Get Categories */
    static getCategories(categoryType = null) {
        const categories = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category);
        if (categoryType) {
            return categories.filter(c => c.type === categoryType);
        }

        return categories;
    }

    /* Get a specific Category */
    static getCategory(categoryID) {
        return this.getCategories().find(c => c.id === categoryID);
    }

    /* Get Custom Tags */
    static getCustomTags() {
        return game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents).customTags;
    }

    /* Get Templates */
    static getTemplates() {
        return game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents).templates;
    }

    static getTemplate(templateID) {
        return this.getTemplates().find(t => t.id === templateID);
    }

    /* Get all Regions */
    static getAllRegions() {
        let regions = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data);
        regions = game.user.isGM ? regions : regions.filter(r => !r.private);

        return regions;
    }

    /* Get a specific Region */
    static async getRegion(regionID) {
        let region = await RegionInfo.init(regionID);
        if (!game.user.isGM && region.private) {
            region = null;
        }

        return region;
    }

    static getRegionByName(regionName) {
        return this.getAllRegions().find(r => r.name === regionName);
    }

    /* Get Associations for a given Region */
    static getRegionAssociations(regionID) {
        let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        return documents.associations.filter(a => a.regionID === regionID);
    }

    /* Get Import Folder Name */
    static getImportFolderName(region, folderType) {
        let regionName = region.name ?? game.i18n.localize("REGIONS.default-name");
        return `${regionName} (${folderType})`;
    }

    /* Update a Category */
    static async updateCategory(category) {
        if (!game.user.isGM) { return; }

        let categories = this.getCategories();
        categories[categories.map((x, i) => [i, x]).filter(x => x[1].id === category.id)[0][0]] = category;
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, categories);
        Hooks.call("regionsUpdateCategory", category);
    }

    /* Update Custom Tags */
    static async updateCustomTags(customTags) {
        if (!game.user.isGM) { return; }

        let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        documents.customTags = customTags;
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documents);

        Hooks.call("regionsUpdateCustomTags", customTags);
    }

    /* Update Template */
    static async updateTemplate(template) {
        if (!game.user.isGM) { return; }

        let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        documents.templates[documents.templates.map((x, i) => [i, x]).filter(x => x[1].id === template.id)[0][0]] = template;

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documents);
        Hooks.call("regionsUpdateTemplate", template);
    }

    /* Update a Region */
    static async updateRegion(region) {
        if (!game.user.isGM) { return; }

        if (!region.associations) { return; }

        let data = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        data.associations = data.associations.filter(a => a.regionID !== region.id);
        data.associations.push.apply(data.associations, region.associations);

        let regionAssociations = data.associations.filter(a => a.foundryID === region.id && a.type === REGIONS_CONFIG.TYPES.REGION);
        for (let ra of regionAssociations) {
            ra.foundryName = region.name;
            ra.foundryIMG = region.icon;
        }

        delete region.associations;
        let regions = this.getAllRegions();

        regions[regions.map((x, i) => [i, x]).filter(x => x[1].id === region.id)[0][0]] = region;

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, data);
        Hooks.call("regionsUpdateRegion", region);

        await RegionsData.updateFolders();
    }

    /**
     * deleteBackupByID - Deletes a local global backup by the given backup ID
     * @param {*} backupID - ID of the local backup to delete.
     */
    static async deleteBackupByID(backupID) {
        if (!game.user.isGM) { return; }
        let backups = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups);
        delete backups[backupID];
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_backups, backups);
    }

    /* Delete an association within a region */
    static async deleteRegionAssociation(regionID, associationID) {
        if (!game.user.isGM) { return; }

        let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        let index = documents.associations.findIndex(d => d.id === associationID && d.regionID == regionID);
        documents.associations.splice(index, 1);

        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documents);
    }

    /* Delete a category */
    static async deleteCategory(categoryID) {
        let category = RegionsData.getCategory(categoryID);
        let regions = RegionsData.getAllRegions();
        let templates = RegionsData.getTemplates();

        switch (category.type) {
            case REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION:
                //ALL associations need to be updated...
                for (let r of regions) {
                    r.associations = RegionsData.getRegionAssociations(r.id);
                    r.associations = r.associations.map(a => {
                        a.categoryID = a.categoryID === categoryID ? null : a.categoryID;
                        return a;
                    })
                    await RegionsData.updateRegion(r);
                }

                for (let t of templates) {
                    t.customTags.map(c => {
                        c.categoryID = c.categoryID === categoryID ? null : c.categoryID;
                        return c;
                    });

                    await RegionsData.updateTemplate(t);
                }
                break;
            case REGIONS_CONFIG.CATEGORY_TYPES.REGION:
                for (let r of regions) { r.categoryID = (r.categoryID === categoryID) ? null : r.categoryID; }
                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);
                break;
            default:
                break;
        }

        let categories = RegionsData.getCategories().filter(c => c.id !== categoryID);
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category, categories);
        Hooks.call("regionsDeleteCategory", category);
    }

    /* Delete a region */
    static async deleteRegion(regionID) {
        if (!game.user.isGM) { return; }

        let region = await this.getRegion(regionID);
        try {
            await Folder.deleteDocuments([region.folders.sceneFolder, region.folders.actorFolder, region.folders.journalFolder]);
        } catch(exception) { }
        
        game.scenes.render(true);
        game.actors.render(true);
        game.journal.render(true);

        //Delete Associations
        let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        documents.associations = documents.associations.filter(a => a.regionID !== regionID);

        let regions = this.getAllRegions().filter(r => r.id !== regionID);
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documents);

        Hooks.call("regionsDeleteRegion", regionID);
    }

    /* Delete a template */
    static async deleteTemplate(templateID) {
        if (!game.user.isGM) { return; }

        let template = RegionsData.getTemplate(templateID);
        let documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
        documents.templates = documents.templates.filter(t => t.id !== templateID);
        await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents, documents);
        Hooks.call("regionsDeleteTemplate", template);
    }
}