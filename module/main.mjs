/* Imports */
import { REGIONS_CONFIG } from "./helpers/config.mjs";
import { Regions } from "./regions.mjs";
import { RegionsAPI } from "./helpers/api.mjs";
import { Logger } from "./logger/logger.mjs";

/* Init Logger */
Logger.MODULE_ID = REGIONS_CONFIG.ID;

/* Initialize */
Hooks.once('init', () => { Regions.initialize(); });

/*Enable Debug Module */
Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
    registerPackageDebugFlag(REGIONS_CONFIG.ID);
});

/* Data Migration on Ready */
Hooks.once('ready', () => { Regions.ready(); });

/* Misc Hooks */
Hooks.on('getSceneNavigationContext', (html, contextOptions) => { Regions.renderNavButton(html, contextOptions) });
Hooks.on('getCompendiumDirectoryEntryContext', (html, options) => { Regions.renderSidebarDirectory(html, options); });
Hooks.on('dropCanvasData', (c, data) => { return Regions.handleCanvasDrop(c, data); });

/* API Hooks */
Hooks.on('updateScene', (scene, activeScene, diffData, messageID) => { RegionsAPI._onRegionsSceneActivate(scene, activeScene, diffData, messageID); });