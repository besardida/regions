import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";

export class RegionsManageCategoriesDialog extends HelpFormApplication {
    #onClose = null;
    #categories = null;
    #hookIndexes = {
        "regionsCreateCategory": null,
        "regionsDeleteCategory": null,
        "regionsUpdateCategory": null,
        "closeApplication": null
    };

    constructor(object, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Categories";
        super(object, options);

        if ( object?.onClose ) {
            this.#onClose = object.onClose;
        }

        this._setupProperties();
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            id: "regionsCategoriesManager",
            height: "auto",
            width: 550,
            submitOnChange: true,
            scrollY: ['.regionsTableList'],
            tabs: [{ navSelector: ".tabs", contentSelector: "#manageCategoryForm" }],
            template: REGIONS_CONFIG.TEMPLATES.MANAGE_CATEGORIES,
            title: game.i18n.localize("REGIONS.CATEGORY.manage")
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    if (dialog.appId === this.appId) {
                        this.validateSubmission();
                    }
                    
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsCreateCategory = Hooks.on("regionsCreateCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteCategory = Hooks.on("regionsDeleteCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsUpdateCategory = Hooks.on("regionsUpdateCategory", this._forceUpdateFromHook.bind(this));
        }
    }

    _setupProperties() {
        this.#categories = [
            {label: game.i18n.localize("REGIONS.CATEGORY.TYPES.region"), type: REGIONS_CONFIG.CATEGORY_TYPES.REGION, documents: RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION)},
            {label: game.i18n.localize("REGIONS.CATEGORY.TYPES.association"), type: REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION, documents: RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION)}
        ];
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
    }

    getData(options) {
        return {
            categories: this.#categories
        }
    }

    async _updateObject(event, formData) {
        let ids = Object.keys(formData);
        let categories = RegionsData.getCategories();
        for (let id of ids) {
            let category = categories.find(c => c.id === id);
            if (category.name !== formData[id].trim()) {
                category.name = formData[id].trim();
                await RegionsData.updateCategory(category);
            }
        }

        this._setupProperties();
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        const type = clickedElement.data().type;
        const associationID = clickedElement.parents('[data-detail-id]')?.data()?.detailId;

        let category = null;

        switch (action) {
            case 'create':
                await RegionsData.createCategory(type);
                break;
            case 'delete-category':
                category = RegionsData.getCategory(associationID);
                let cName = category.name;
                let content = `${game.i18n.localize("REGIONS.delete-category-confirm")} ${cName}?`;
                let fDelete = async () => {
                    await RegionsData.deleteCategory(associationID);
                    this._setupProperties();
                    this.render(true);
                }
                new Dialog({
                    title: game.i18n.localize("REGIONS.delete-category"),
                    content: content,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("REGIONS.delete-category"),
                            callback: (html) => {
                                fDelete();
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case 'cancel':
                this.close();
                break;
            case 'submit':
                this.validateSubmission();
                break;
            default:
                break;
        }

        return true;
    }

    async validateSubmission() {
        if (this.#onClose) {
            this.#onClose();
        }

        this.close();
    }

    async _forceUpdateFromHook(template) {
        this._setupProperties();
        this.render(true);
    }
}