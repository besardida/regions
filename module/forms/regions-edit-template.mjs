import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { RegionsTemplate } from "../models/region-template.mjs";
import { RegionsManageCategoriesDialog } from "./regions-manage-categories.mjs";
import { RegionsManageCustomDialog } from "./regions-manage-custom.mjs";
import { RegionTransferData } from "../models/region-transfer-data.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";
import { RegionsEditCustomDialog } from "./regions-edit-custom.mjs";

export class RegionsEditTemplateDialog extends HelpFormApplication {
    regionTemplate = null;
    #hookIndexes = {
        "regionsUpdateCategory": null,
        "regionsCreateCategory": null,
        "regionsDeleteCategory": null,
        "regionsCreateCustomTag": null,
        "regionsUpdateCustomTags": null,
        "closeApplication": null
    };

    constructor(object = {}, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Edit-Template";
        if (object.readonly) { if (!options) { options = {}; } options.height = 570; }
        super(object, options);

        this.regionTemplate = null;
        this.isEditing = true;
        this.readonly = object.readonly ?? false;

        if (object.template) {
            this.regionTemplate = duplicate(object.template);
            this.isEditing = !object.isNew ?? this.isEditing;
        } else {
            //New resource
            this.isEditing = false;
            this.regionTemplate = new RegionsTemplate();
        }

        this.emptyCategories = [];
        this.regionCategories = [];
        this.availableCategories = [];
        this.availableTags = [];
        this.categories = [];
        this.tags = [];
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: "auto",
            height: 613,
            width: 650,
            submitOnChange: true,
            classes: ["regionsEditTemplateDialog"],
            scrollY: ['.templateTags'],
            dragDrop: [{ dropSelector: null, dragSelector: '.regionsDraggable' }],
            template: REGIONS_CONFIG.TEMPLATES.EDIT_TEMPLATE,
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }


    get title() {
        if (this.readonly) {
            return game.i18n.localize("REGIONS.TEMPLATE.View");
        } else if (this.isEditing) {
            return game.i18n.localize("REGIONS.TEMPLATE.Edit");
        }

        return game.i18n.localize("REGIONS.TEMPLATE.Create");
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsUpdateCategory = Hooks.on("regionsUpdateCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsCreateCategory = Hooks.on("regionsCreateCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteCategory = Hooks.on("regionsDeleteCategory", this._categoryDeleted.bind(this));
            this.#hookIndexes.regionsUpdateCustomTags = Hooks.on("regionsUpdateCustomTags", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsCreateCustomTag = Hooks.on("regionsCreateCustomTag", this._forceUpdateFromHook.bind(this));
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
        html.on('change', '[data-change]', this._handleSelectChange.bind(this));
    }

    getData(options) {
        this.regionCategories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
        this.categories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);
        this.tags = RegionsData.getCustomTags();

        this.availableCategories = duplicate(this.categories);
        this.availableTags = duplicate(this.tags);
        let dataTags = [];

        for (let t of this.regionTemplate.customTags) {
            let index = this.availableCategories.findIndex((c) => { return c.id === t.categoryID; });
            if (index != -1) { this.availableCategories.splice(index, 1); }

            index = this.emptyCategories.findIndex((c) => { return c.id === t.categoryID });
            if (index != -1) { this.emptyCategories.splice(index, 1); }

            index = this.availableTags.findIndex((a) => { return a.id === t.id; });
            if (index != -1) { this.availableTags.splice(index, 1); }

            if (t.categoryID) {
                index = dataTags.findIndex((c) => { return c.id === t.categoryID; });
                if (index != -1) {
                    dataTags[index].documents.push(t);
                } else {
                    dataTags.push({
                        id: t.categoryID,
                        readonly: false,
                        name: this.categories.find(c => c.id === t.categoryID).name,
                        documents: [t]
                    });
                }
            }
        }

        for (let c of this.emptyCategories) {
            dataTags.push({
                id: c.id,
                readonly: false,
                name: c.name,
                documents: null
            });

            let index = this.availableCategories.findIndex((a) => { return a.id === c.id});
            if (index != -1) { this.availableCategories.splice(index, 1); }
        }

        dataTags.push({
            id: null,
            readonly: true,
            name: game.i18n.localize("REGIONS.uncategorized"),
            documents: this.regionTemplate.customTags.filter(t => !t.categoryID)
        });

        return {
            regionTemplate: this.regionTemplate,
            regionCategories: this.regionCategories,
            availableCategories: this.availableCategories,
            emptyCategories: this.emptyCategories,
            availableTags: this.availableTags,
            templateTags: dataTags,
            readonly: this.readonly,
        }
    }

    async _updateObject(event, formData) {
        this.regionTemplate.name = formData.name;
        this.regionTemplate.shortDescription = formData.shortDescription;
        this.regionTemplate.categoryID = formData.regionCategory === "null" ? null : formData.regionCategory;
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;

        let dialog = null;
        let detailID = clickedElement.parents('[data-detail-id]')?.data()?.detailId;
        let needsRefresh = false;
        let _this = this;

        switch (action) {
            case "expand-collapse":
                this._expandCollapse(clickedElement);
                break;
            case "manage-categories":
                dialog = new RegionsManageCategoriesDialog();
                dialog.render(true);
                break;
            case "manage-custom":
                dialog = new RegionsManageCustomDialog();
                dialog.render(true);
                break;
            case "delete-category":
                detailID = clickedElement.data()?.categoryId;
                for (let tag of this.regionTemplate.customTags) {
                    tag.categoryID = tag.categoryID === detailID ? null : tag.categoryID;
                }
                this.emptyCategories = this.emptyCategories.filter(e => e.ID !== detailID);
                needsRefresh = true;
                break;
            case "delete-custom":
                this.regionTemplate.customTags = this.regionTemplate.customTags.filter(t => t.id !== detailID);
                needsRefresh = true;
                break;
            case "edit-custom":
                let customTag = this.regionTemplate.customTags.find(c => c.id === detailID);
                dialog = new RegionsEditCustomDialog(customTag);
                dialog.onClose = (customTag) => { _this.onCustomTagClose(customTag); };
                dialog.render(true);
                break;
            case 'icon':
                let filePicker = new FilePicker({
                    type: "image",
                    current: event.currentTarget.dataset.icon,
                    callback: function (newFile) {
                        _this.regionTemplate.icon = newFile;
                        event.currentTarget.dataset.icon = event.currentTarget.src = newFile;
                    }
                });
    
                filePicker.render();
    
                break;
            case "submit":
                if (!this.isEditing) { 
                    await RegionsData.createTemplate(this.regionTemplate);
                } else {
                    await RegionsData.updateTemplate(this.regionTemplate);
                }

                this.close();
                break;
            case "cancel":
                this.close();
                break;
            default:
                break;
        }

        if (needsRefresh) { this.render(true); }
    }

    async _handleSelectChange(event) {
        let action = event.target.dataset?.change;
        let valueID = event.target.value;

        if (!valueID) { return false; } //Please select!
        let dialog = null;

        switch (action) {
            case "add-custom-tag":
                let customTag = this.tags.find(t => t.id === valueID);
                this.regionTemplate.customTags.push(customTag);
                break;
            case "add-existing-category":
                let category = this.categories.find(c => c.id === valueID);
                this.emptyCategories.push(category);
                break;
            default:
                break;
        }

        this.render(true);
    }

    _categoryDeleted = (object) => {
        for (let c of this.regionTemplate.customTags) {
            c.categoryID = c.categoryID === object.id ? null : c.categoryID;
        }

        this.render(true);
    }
    
    _forceUpdateFromHook = (object) => {
        this.render(true);
    }

    _expandCollapse(clickedElement) {
        let contentDiv = clickedElement.parents('.regionsCategoryHeader').siblings('.regionsCategoryContent');
        let icon = clickedElement.children('.fas');

        contentDiv.toggleClass('open');
        contentDiv.slideToggle();
        icon.toggleClass('fa-caret-square-down');
        icon.toggleClass('fa-caret-square-up');
    }

    onCustomTagClose(customTag) {
        let tag = this.regionTemplate.customTags.find(ct => ct.id === customTag.id);
        tag = customTag;
        this.render(true);
    }

    async _onDragStart(event) {
        if (!game.user.isGM) { return; }

        let detailID = $(event.target).closest("[data-detail-id]")[0]?.dataset?.detailId;
        let customTag = this.regionTemplate.customTags.find(t => t.id === detailID);
        let transferData = new RegionTransferData({
            internalID: detailID,
            object: customTag,
            transferType: REGIONS_CONFIG.TYPES.CUSTOM
        });

        event.dataTransfer.setData("text/plain", JSON.stringify(transferData));
    }

    async _onDrop(event) {
        if (!game.user.isGM) { return; }

        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData("text/plain"));
        } catch (err) {
            return false;
        }

        if (data.source === REGIONS_CONFIG.ID && data.transferType === REGIONS_CONFIG.TYPES.CUSTOM) {
            const newCategoryID = $(event.target).closest("[data-category-id]")[0]?.dataset?.categoryId;

            let customTag = this.regionTemplate.customTags.find(t => t.id === data.internalID);
            if (customTag && customTag.categoryID === newCategoryID) {
                return false;
            } else if (customTag) {
                let index = this.regionTemplate.customTags.findIndex(t => t.id === data.internalID);
                if (index !== -1) { this.regionTemplate.customTags.splice(index, 1); }
            } else {
                customTag = data.object;
            }

            customTag.categoryID = newCategoryID;
            this.regionTemplate.customTags.push(customTag);
            this.render(true);
        }
    }
}