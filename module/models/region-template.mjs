import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";

export class RegionsTemplate {
    id = null;
    name = "";
    shortDescription = "";
    categoryID = null;
    customTags = [];
    icon = "icons/tools/navigation/map-marked-blue.webp";

    constructor() {
        this.id = foundry.utils.randomID(16);
    }
}