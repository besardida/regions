import { REGIONS_CONFIG } from "../helpers/config.mjs";

/** 
 * @typedef {Object} RegionCustomTag
 * @property {String} id - internal id for this tag
 * @property {String} regionID - region id this custom tag belongs to
 * @property {String} type - what type this custom tag is, defined from Regions Constants
 * @property {Object} value - the current value of this object, type of value varies based on type of custom tag
 * @property {Array} options - optional list of options for List types
 * @property {Array} steps - Array for storing true/false values for counters
 * @property {Number} max - Maximum value for numeric/counters
 * @property {String} name - User created name for this custom tag
 * @property {String} categoryID - ID of the category this custom tag belongs to
 * @property {String} globalID - The ID of the Custom Tag this was created from
 */
export class RegionsCustomTag {
    id = null;
    regionID = null;
    type = REGIONS_CONFIG.KEY_TYPES.TEXT;
    value = null;
    options = [];
    steps = [];
    max = 5;
    name = "";
    categoryID = null;
    globalID = null;

    constructor() {
        this.id = foundry.utils.randomID(16);
    }
}