import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionTransferData } from "../models/region-transfer-data.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { RegionAssociation } from "../models/region-association.mjs";
import { RegionsEditCustomDialog } from "./regions-edit-custom.mjs";
import { RegionsManageCustomDialog } from "./regions-manage-custom.mjs";
import { RegionInfo } from "../models/region-info.mjs";
import { RegionsManageCategoriesDialog } from "./regions-manage-categories.mjs";
import { RegionsEditTemplateDialog } from "./regions-edit-template.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";
import { Logger } from "../logger/logger.mjs";

export class RegionsEditRegionDialog extends HelpFormApplication {
    #hookIndexes = {
        "regionsCreateCustomTag": null,
        "regionsUpdateCustomTag": null,
        "regionsDeleteCustomTag": null,
        "regionsUpdateCategory": null,
        "regionsCreateCategory": null,
        "regionsDeleteCategory": null,
        "regionsUpdateRegion": null,
        "regionsDeleteRegion": null,
        "closeApplication": null
    };

    constructor(object, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Edit-Region";
        super(object, options);

        this.region = object.region;
        this.activeTab = 'info';

        this.onSaveCallback = object.onSaveCallback;
        this.searchFilter = null;
        this.availableTags = [];

        this.emptyCategories = {};
        for (let c of Object.keys(REGIONS_CONFIG.TYPES)) {
            this.emptyCategories[REGIONS_CONFIG.TYPES[c]] = [];
        }

        this.availableCategories = deepClone(this.emptyCategories);
        this.templates = [];

        this.setupProperties();
    }

    get id() {
        return `region-${this.region.id}`;
    }

    get title() {
        const localizeName = game.user.isGM ? "REGIONS.edit-region" : "REGIONS.view-region";

        return `${game.i18n.localize(localizeName)} - ` + (this.region.name ?? game.i18n.localize("REGIONS.default-name"));
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: 'auto',
            classes: ['regions', 'regionDetail'],
            submitOnChange: true,
            template: REGIONS_CONFIG.TEMPLATES.REGION,
            tabs: [{ navSelector: ".tabs", contentSelector: ".regionContent", initial: "info" }],
            dragDrop: [{ dropSelector: null, dragSelector: '.regionsDraggable' }],
            editable: game.user.isGM,
            scrollY: [
                ".regionDetailContent"
            ]
        }

        const mergedOptions = foundry.utils.mergeObject(defaults, overrides);

        return mergedOptions;
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsUpdateCategory = Hooks.on("regionsUpdateCategory", this._categoriesUpdated.bind(this));
            this.#hookIndexes.regionsCreateCategory = Hooks.on("regionsCreateCategory", this._categoriesUpdated.bind(this));
            this.#hookIndexes.regionsDeleteCategory = Hooks.on("regionsDeleteCategory", this._categoriesUpdated.bind(this));
            this.#hookIndexes.regionsCreateCustomTag = Hooks.on("regionsCreateCustomTag", this._customTagsUpdated.bind(this));
            this.#hookIndexes.regionsUpdateCustomTag = Hooks.on("regionsUpdateCustomTag", this._customTagsUpdated.bind(this));
            this.#hookIndexes.regionsDeleteCustomTag = Hooks.on("regionsDeleteCustomTag", this._customTagsUpdated.bind(this));
            this.#hookIndexes.regionsUpdateRegion = Hooks.on("regionsUpdateRegion", this._regionUpdated.bind(this));
            this.#hookIndexes.regionsDeleteRegion = Hooks.on("regionsDeleteRegion", this._regionDeleted.bind(this));
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        let _this = this;
        html.on('click', "[data-action]", this._handleButtonClick.bind(this));
        html.on('change', "[data-change]", this._handleSelectionChange.bind(this));
    }

    getData(options) {
        const regionConfigData = {
            region: this.region,
            notes: this.region.notes,
            options: options,
            title: this.title,
            availableTags: this.availableTags,
            custom: this.custom,
            regions: this.regions,
            scenes: this.scenes,
            actors: this.actors,
            items: this.items,
            journals: this.journals,
            rolltables: this.rolltables,
            playlists: this.playlists,
            compendiums: this.compendiums,
            searchLists: this.searchLists,
            searchFilter: this.searchFilter,
            editable: game.user.isGM,
            owner: game.user.isGM,
            categories: this.availableCategories,
            templates: this.templates,
            isAtLeastFoundryV10: isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10")
        };

        return regionConfigData;
    }

    _onChangeTab(event, tabs, active) {
        super._onChangeTab(event, tabs, active);
        this.activeTab = active;
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        const associationID = clickedElement.parents('[data-detail-id]')?.data()?.detailId;
        let association = associationID != null ? this.region.getAssociationByID(associationID) : null;

        let _this = this;
        let dialog = null;
        let needRefresh = false;

        switch (action) {
            case "expand-collapse":
                this.expandCollapse(clickedElement);
                break;
            case 'icon':
                let filePicker = new FilePicker({
                    type: "image",
                    current: event.currentTarget.dataset.icon,
                    callback: function (newFile) {
                        _this.region.icon = newFile;
                        event.currentTarget.dataset.icon = event.currentTarget.src = newFile;
                        _this.saveRegion();
                    }
                });

                filePicker.render();

                break;
            case 'manage-custom':
                dialog = new RegionsManageCustomDialog();
                dialog.render(true);
                break;
            case 'manage-categories':
                dialog = new RegionsManageCategoriesDialog();
                dialog.render(true);
                break;
            case 'edit-custom':
                dialog = new RegionsEditCustomDialog(association);
                dialog.onClose = (customTag) => { _this.onCustomTagClose(customTag); };
                dialog.render(true);
                break;
            case 'delete-custom':
                this.region.associations = this.region.associations.filter(a => a.id !== associationID);
                needRefresh = true;
                break;
            case 'custom-counter-adjust':
                let stepIndex = clickedElement.data().step;
                let newValue = !association.customData.steps[stepIndex];
                if (stepIndex < association.customData.max) {
                    if (association.customData.steps[stepIndex + 1] === true) {
                        newValue = true;
                    }
                }
                for (let i = 0; i < stepIndex; i++) { association.customData.steps[i] = true; }
                association.customData.steps[stepIndex] = newValue;
                association.customData.steps = association.customData.steps.map((val, index) => { return (index > stepIndex ? false : val)});

                needRefresh = true;
                break;
            case 'custom-tag-decrease':
                association.customData.value--;
                needRefresh = true;
                break;
            case 'custom-tag-increase':
                (association.customData.max === 0 || (association.customData.max !== 0 && association.customData.value < association.customData.max)) && association.customData.value++;
                needRefresh = true;
                break;
            case 'view':
                association.renderAssociationSheet();
                break;
            case 'scenes':
                association.viewScene();
                break;
            case 'rolltables':
                association.rollTable();
                break;
            case 'playlists':
                association.play();
                break;
            case 'deleteAssociation':
                this.region.associations = this.region.associations.filter(a => a.id !== associationID);

                needRefresh = true;
                break;
            case 'search':
                this.performSearch();
                break;
            case 'clearSearch':
                this.performSearch(true);
                break;
            case 'toggle-collapse':
                let contentDiv = clickedElement.parents('.regionsCategoryHeader').siblings('.regionsCategoryContent');
                let icon = clickedElement.children('.fas');

                contentDiv.toggleClass('open');
                contentDiv.slideToggle();
                icon.toggleClass('fa-caret-square-down');
                icon.toggleClass('fa-caret-square-up');
                break;
            case 'delete-category':
                let categoryID = clickedElement.closest("[data-category-id]").data().categoryId;

                for (let key of Object.keys(this.emptyCategories)) {
                    this.emptyCategories[key] = this.emptyCategories[key].filter(ec => ec.id !== categoryID);
                }
                
                this.region = await RegionsData.getRegion(this.region.id); //We need to refresh the local region data.

                needRefresh = true;
                break;
            case 'createDocument':
                let createType = clickedElement.data().type;
                await this.createNewOfType(createType);
                if (createType === 'Region') {
                    needRefresh = true;
                }
                break;
            case 'open-template':
                dialog = new RegionsEditTemplateDialog({template: this.region.template, readonly: true});
                dialog.render(true);
                break;
            case 'remove-template':
                new Dialog({
                    title: game.i18n.localize("REGIONS.TEMPLATE.Remove"),
                    content: game.i18n.localize("REGIONS.TEMPLATE.RemoveAllOrJustTemplate"),
                    buttons: {
                        all: {
                            icon: "<i class='fas fa-trash'></i>",
                            label: game.i18n.localize("REGIONS.TEMPLATE.RemoveAll"),
                            callback: async () => {
                                let templateIDs = _this.region.template.customTags.map((ct) => { return ct.globalID ?? ct.id; });
                                _this.region.associations = _this.region.associations.filter(a => a.type !== 'Custom' || (a.type === 'Custom' && templateIDs.indexOf(a.customData.globalID) === -1));
                                _this.region.template = null;
                                await _this.saveRegion();
                            }
                        },
                        remove: {
                            icon: "<i class='fas fa-remove-format'></i>",
                            label: game.i18n.localize("REGIONS.TEMPLATE.RemoveTemplate"),
                            callback: async () => {
                                _this.region.template = null;
                                await _this.saveRegion();
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.cancel")
                        }
                    },
                    default: "no"
                } 
                ,{
                    classes: ["dialog", "regionsConfirmDialog"]
                }
                ).render(true);
            default:
                association?.renderAssociationSheet();
                break;
        }

        if (needRefresh) {
            await this.saveRegion();
        }
    }

    async _handleSelectionChange(event) {
        let changeAction = event.target.dataset.change;
        switch (changeAction) {
            case "add-custom-tag":
                await this.addCustomTag(event.target.value);
                break;
            case "add-existing-category":
                let category = this.categories.find(c => c.id === event.target.value);
                this.addCategoryToEmptyList(category);
                this.setupProperties();
                this.render();
                break;
            default:
                break;
        }
    }

    setupProperties() {
        let _this = this;

        this.categories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);
        for (let key of Object.keys(this.availableCategories)) {
            this.availableCategories[key] = deepClone(this.categories);
        }

        this.availableTags = RegionsData.getCustomTags();

        this.custom = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.CUSTOM);
        this.scenes = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.SCENE);
        this.actors = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.ACTOR);
        this.items = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.ITEM);
        this.journals = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.JOURNAL);
        this.rolltables = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.ROLLTABLE);
        this.playlists = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.PLAYLIST);
        this.regions = this.region.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.REGION);

        if (this.custom) {
            for (let c of this.custom) {
                let index = this.availableTags.findIndex(a => a.id === c.customData.id);
                if (index != -1) {
                    this.availableTags.splice(index, 1);
                }
            }
        }

        this.searchLists = {
            custom: this.sortDocumentsByCategory(Array.from(this.custom).filter(f => _this.searchFilter ? f.customData?.name.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.CUSTOM),
            regions: this.sortDocumentsByCategory(Array.from(this.regions).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.REGION),
            scenes: this.sortDocumentsByCategory(Array.from(this.scenes).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.SCENE),
            actors: this.sortDocumentsByCategory(Array.from(this.actors).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.ACTOR),
            items: this.sortDocumentsByCategory(Array.from(this.items).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.ITEM),
            journals: this.sortDocumentsByCategory(Array.from(this.journals).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.JOURNAL),
            rolltables: this.sortDocumentsByCategory(Array.from(this.rolltables).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.ROLLTABLE),
            playlists: this.sortDocumentsByCategory(Array.from(this.playlists).filter(f => _this.searchFilter ? f.foundryName.includes(_this.searchFilter) : f), game.user.isGM, REGIONS_CONFIG.TYPES.PLAYLIST)
        }

        this.custom = this.sortDocumentsByCategory(this.custom, game.user.isGM, REGIONS_CONFIG.TYPES.CUSTOM);
        this.regions = this.sortDocumentsByCategory(this.regions, game.user.isGM, REGIONS_CONFIG.TYPES.REGION);
        this.scenes = this.sortDocumentsByCategory(this.scenes, game.user.isGM, REGIONS_CONFIG.TYPES.SCENE);
        this.actors = this.sortDocumentsByCategory(this.actors, game.user.isGM, REGIONS_CONFIG.TYPES.ACTOR);
        this.items = this.sortDocumentsByCategory(this.items, game.user.isGM, REGIONS_CONFIG.TYPES.ITEM);
        this.journals = this.sortDocumentsByCategory(this.journals, game.user.isGM, REGIONS_CONFIG.TYPES.JOURNAL);
        this.rolltables = this.sortDocumentsByCategory(this.rolltables, game.user.isGM, REGIONS_CONFIG.TYPES.ROLLTABLE);
        this.playlists = this.sortDocumentsByCategory(this.playlists, game.user.isGM, REGIONS_CONFIG.TYPES.PLAYLIST);
    }

    async createNewOfType(type) {
        let association = new RegionAssociation({
            type: type,
            regionID: this.region.id
        });
        let _this = this;

        let promise = null;

        switch (type) {
            case REGIONS_CONFIG.TYPES.ACTOR:
                promise = Actor.createDialog({folder: this.region.folders.actorFolder});
                break;
            case REGIONS_CONFIG.TYPES.ITEM:
                promise = Item.createDialog();
                break;
            case REGIONS_CONFIG.TYPES.JOURNAL:
                promise = JournalEntry.createDialog({folder: this.region.folders.journalFolder});
                break;
            case REGIONS_CONFIG.TYPES.PLAYLIST:
                promise = Playlist.createDialog();
                break;
            case REGIONS_CONFIG.TYPES.ROLLTABLE:
                promise = RollTable.createDialog();
                break;
            case REGIONS_CONFIG.TYPES.SCENE:
                promise = Scene.createDialog({folder: this.region.folders.sceneFolder});
                break;
            case REGIONS_CONFIG.TYPES.REGION:
                let newRegion = await RegionsData.createRegion();
                association.foundryIMG = newRegion.icon;
                association.foundryName = game.i18n.localize("REGIONS.default-name");
                this.region.associations.push(association);
                break;
            default:
                break;
        }

        if (promise) {
            promise.then(result => {
                if (result) {
                    association.foundryID = result.id;
                    association.foundryName = result.name;
                    association.foundryIMG = result.img;
                    _this.region.associations.push(association);
                    _this.setupProperties();
                    _this.render();
                }
            });
        }
    }

    async addCustomTag(tagID) {
        let tag = this.availableTags.find(a => a.id === tagID);
        let association = new RegionAssociation({regionID: this.region.id, customData: tag});
        this.region.associations.push(association);
        await this.saveRegion();
    }

    async addNewCategory() {
        let newCategory = await RegionsData.createCategory(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);
        this.addCategoryToEmptyList(newCategory);
    }

    addCategoryToEmptyList(category) {
        let categoryName = "";

        switch (this.activeTab) {
            case 'custom':
                categoryName = REGIONS_CONFIG.TYPES.CUSTOM;
                break;
            case 'regions':
                categoryName = REGIONS_CONFIG.TYPES.REGION;
                break;
            case 'scenes':
                categoryName = REGIONS_CONFIG.TYPES.SCENE;
                break;
            case 'actors':
                categoryName = REGIONS_CONFIG.TYPES.ACTOR;
                break;
            case 'items':
                categoryName = REGIONS_CONFIG.TYPES.ITEM;
                break;
            case 'journals':
                categoryName = REGIONS_CONFIG.TYPES.JOURNAL;
                break;
            case 'rolltables':
                categoryName = REGIONS_CONFIG.TYPES.ROLLTABLE;
                break;
            case 'playlists':
                categoryName = REGIONS_CONFIG.TYPES.PLAYLIST;
                break;
            default:
                break;
        }

        if (!this.emptyCategories[categoryName]?.find(e => e === category)) {
            this.emptyCategories[categoryName].push(category);
        }
    }

    performSearch(clearSearch = false) {
        const formData = this._getSubmitData();
        this.searchFilter = clearSearch ? null : formData.searchFilter;
        this.setupProperties();
        this.render(true);
    }

    expandCollapse(clickedElement) {
        let targetContent = clickedElement.data().target;
        let contentDiv = clickedElement.siblings(`.${targetContent}`);
        let icon = clickedElement.children('button').children('.fas');

        contentDiv.toggleClass('open');
        contentDiv.slideToggle();
        icon.toggleClass('fa-caret-square-down');
        icon.toggleClass('fa-caret-square-up');
    }

    _categoriesUpdated(category) {
        this.setupProperties();
        this.render(true);
    }

    _customTagsUpdated(customTags) {
        this.setupProperties();
        this.render(true);
    }

    async _regionUpdated(region) {
        if (region.id !== this.region.id) { return; }

        this.region = await RegionInfo.init(region.id);
        this.setupProperties();
        this.render(true);
    }

    _regionDeleted(regionID) {
        if (regionID !== this.region.id) { return; }

        this.close({force: true});
    }

    sortDocumentsByCategory(documents, isOwner, documentType) {
        let data = [];

        if (!documents?.length) {
            return [];
        }

        for (let c of this.categories) {
            let d = documents.filter(document => document.categoryID === c.id);
            if (d?.length) {
                this.availableCategories[documentType] = this.availableCategories[documentType].filter(u => u.id !== c.id);
                data.push({
                    id: c.id,
                    readonly: !isOwner,
                    name: c.name,
                    documents: d
                });
            }
        }

        let categoriesToRemove = [];

        for (let c of this.emptyCategories[documentType]) {
            if (data.find(d => d.id === c.id)) {
                categoriesToRemove.push(c.id);
            } else {
                this.availableCategories[documentType] = this.availableCategories[documentType].filter(u => u.id !== c.id);
                data.push({
                    id: c.id,
                    readonly: !isOwner,
                    name: c.name,
                    documents: []
                });
            }
        }

        this.emptyCategories[documentType] = this.emptyCategories[documentType].filter(e => categoriesToRemove.indexOf(e.id) === -1);

        data.push({
            id: null,
            readonly: true,
            name: game.i18n.localize("REGIONS.uncategorized"),
            documents: documents.filter(d => !d.categoryID)
        });

        return data;
    }

    async saveRegion() {
        const formData = this._getSubmitData();
        if (formData.hasOwnProperty("notes")) {
            this.region.notes = formData.notes;
        }

        await RegionsData.updateRegion(this.region);
        this.region = await RegionInfo.init(this.region.id);
        this.setupProperties();
        this.render();
    }

    async _updateObject(event, formData) {
        if (event.target === 'input.regionsCustomCounter') { return; } //No form changes to update
        if (typeof (event.target === 'object') && event.target?.name === 'regionsExistingCategories' || event.target?.name === 'regionsExistingCustomTag') { return; } //No form changes to update

        this.region.name = formData.name;
        this.region.private = formData.private;
        this.searchFilter = formData.searchFilter;

        let formKeys = Object.keys(formData);

        //Custom Key Checks
        if (formData.regionsCustomID) {
            if (typeof (formData.regionsCustomID) === "object") {
                //Get Custom Keys
                for (let i = 0; i < formData.regionsCustomID.length; i++) {
                    let category = this.custom.find(c => c.documents.find(d => d.id === formData.regionsCustomID[i]));
                    let custom = category.documents.find(d => d.id === formData.regionsCustomID[i]).customData;

                    let value = formData[`regionsCustomValue-${formData.regionsCustomID[i]}`];
                    if (custom.type === 'NUMERIC') {
                        if (custom.max !== 0 && value > custom.max) {
                            value = custom.max;
                        }
                        custom.value = value;
                    } else if (custom.type === 'TOGGLE' && value.length === 1) {
                        custom.value = !custom.value;
                    } else {
                        custom.value = value;
                    }

                }
            } else {
                //Only 1 key
                let category = this.custom.find(c => c.documents.find(d => d.id === formData.regionsCustomID));
                let custom = category.documents.find(d => d.id === formData.regionsCustomID).customData;
                let value = formData[`regionsCustomValue-${formData.regionsCustomID}`];
                if (custom.type === 'NUMERIC') {
                    if (value > custom.max) {
                        value = custom.max;
                    }
                    custom.value = value;
                } else if (custom.type === 'TOGGLE' && value.length === 1) {
                    custom.value = !custom.value;
                } else {
                    custom.value = value;
                }
            }
        }

        //Description Checks
        let descriptions = formKeys.filter(fk => fk.startsWith("regionDescription-"));
        if (descriptions) {
            if (typeof(descriptions) === 'string') {
                descriptions = [descriptions];
            }
            for (let description of descriptions) {
                let associationID = description.replace("regionDescription-", "");
                if (associationID && this.region.associations) {
                    let association = this.region.associations.find(a => a.id === associationID);
                    association.description = formData[description];
                }
            }
        }

        if (formData.notes) {
            this.region.notes = formData.notes;
        }

        await this.saveRegion();
    }

    async _onDragStart(event) {
        //Get the association we're dragging
        let detailID = $(event.target).closest("[data-detail-id]").data().detailId;
        const association = this.region.getAssociationByID(detailID);
        let transferData = new RegionTransferData({
            transferType: association.type,
            id: association.foundryID,
            pack: null,
            object: association
        });

        //If it's from a compendium, adjust to check if they've already been imported
        if (association.compendiumKey) {
            switch (association.type) {
                case REGIONS_CONFIG.TYPES.ACTOR:
                    let actor = game.actors.getName(association.foundryName);
                    if (actor) {
                        transferData.id = actor.id;
                    } else {
                        //Nope, but that's ok - set the pack key and let Foundry handle the import
                        transferData.pack = association.compendiumKey;
                    }
                    break;
                case REGIONS_CONFIG.TYPES.JOURNAL:
                    //Journals MUST be imported to be used elsewhere!
                    let journal = game.journal.getName(association.foundryName);
                    if (journal) {
                        transferData.id = journal.id;
                    } else {
                        //Doesn't exist; it needs to be imported first.
                        transferData.pack = association.compendiumKey;
                    }
                    break;
                default:
                    //By default it's fine if it is purely from a compendium
                    transferData.pack = association.compendiumKey;
                    break;
            }
        } else if (association.type === REGIONS_CONFIG.TYPES.CUSTOM) {
            transferData.object = duplicate(this.region.associations.find(a => a.id === detailID));
        }

        Logger.debug("Transferring Data", transferData);

        //Update the eventdata
        event.dataTransfer.setData("text/plain", JSON.stringify(transferData));
    }

    async _onDrop(event) {
        if (!game.user.isGM || !this.region.associations) { return; }

        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData("text/plain"));
        } catch (err) {
            return false;
        }

        let dropCategoryID = $(event.target).closest(".regionsCategoryHeader")?.parent()?.data()?.categoryId;
        if (!dropCategoryID) {
            dropCategoryID = $(event.target).closest(".regionsCategoryContent")?.parent()?.data()?.categoryId;
        }
        let tabName = null;
        let docType = data.type ?? data.transferType;
        let newAssociations = [];
        let existingAssociations = [];

        if (docType === REGIONS_CONFIG.TYPES.TEMPLATE) {
            let template = data.object;
            await this.region.applyTemplate(template);
            return;
        }
        
        if (docType === REGIONS_CONFIG.TYPES.FOLDER) {
            let folderData = await Promise.all(await this._handleDropFolder(data, dropCategoryID));
            for (let d of folderData) {
                tabName = d.tabName;
                if(d.isExisting) {
                    existingAssociations.push(d.association);
                } else {
                    newAssociations.push(d.association);
                }
            }
        } else {
            let associationData = await RegionAssociation.buildAssociationFromData(this.region, data, dropCategoryID);
            tabName = associationData.tabName;
            if (associationData.isExisting) {
                existingAssociations.push(associationData.association);
            } else {
                newAssociations.push(associationData.association);
            }
        }

        if (newAssociations.length > 0) {
            this.region.associations = this.region.associations.concat(newAssociations);
        }

        if (existingAssociations.length > 0) {
            for(let assoc of existingAssociations) {
                //This is potentially where the issue is happening!!!!
                let association = this.region.associations.find(a => a.id === assoc.id);
                association.categoryID = assoc.categoryID;
                association.foundryIMG = assoc.foundryIMG;
                association.foundryName = assoc.foundryName;
            }
        }

        this._tabs[0].activate(tabName);
        this.activeTab = tabName;
        await this.saveRegion();
    }

    async _handleDropFolder(data, categoryID) {
        const folder = game.folders.get(data.id);

        let associations = await Promise.all(folder.contents.map(async (f) => {
            let image = f.data.thumb ? f.data.thumb : f.data.img ? f.data.img : null;
            if (!image) {
                switch (folder.data.type) {
                    case "Journal":
                        image = "icons/svg/book.svg";
                        break;
                    case "Playlist":
                        image = "icons/svg/sound.svg";
                        break;
                }
            }

            return await RegionAssociation.buildAssociationFromData(this.region, {
                id: f.data.id ?? f.data._id,
                type: folder.data.type,
                data: { thumb: image, img: image },
                name: f.data.name
            }, categoryID);
        }));

        //Last thing we do is go recursive and see if we can get more folders!
        for (let f of folder.children) {
            const newData = { id: f.data._id, documentName: f.data.documentName, type: data.type};
            associations = associations.concat(await Promise.all(await this._handleDropFolder(newData, categoryID)));
        }

        return associations;
    }

    async onCustomTagClose(customTag) {
        let tag = this.region.associations.find(a => a.id === customTag.id);
        if (tag) {
            tag.customData = customTag;
        } else {
            this.region.associations.push(customTag);
        }

        await this.saveRegion();
    }
}