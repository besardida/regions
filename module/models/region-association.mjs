import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { Logger } from "../logger/logger.mjs";

/**
 * @typedef {Object} RegionAssociation 
 * @property {string} type - what type of entity this association is
 * @property {string} id - Internal id of this association
 * @property {string} foundryID - Foundry's internal id.  Can be null.
 * @property {string} compendiumKey - The compendium this association is part of.  Can be null
 * @property {string} compendiumID - The known compendium ID this association has.  Can be null
 * @property {string} foundryIMG - The image path/data used by Foundry
 * @property {string} foundryName - The name of the object in Foundry
 * @property {string} regionID - The id of the region association with this object
 * @property {string} description - A brief description shown in the Regions UI
 * @property {string} categoryID - The category ID that this association belongs to.
 * @property {object} customData - data available for custom tags.
 * @property {number} sortOrder - Order priority of this association.  Lower means on display first.
 */
export class RegionAssociation {
    constructor(defaults = {}) {
        this.id = defaults.id ?? foundry.utils.randomID(16);
        this.type = defaults.type ?? REGIONS_CONFIG.TYPES.CUSTOM;
        this.foundryID = defaults.foundryID ?? null;
        this.compendiumKey = defaults.compendiumKey ?? null;
        this.compendiumID = defaults.compendiumID ?? null;
        this.foundryIMG = defaults.foundryIMG ?? null;
        this.foundryName = defaults.foundryName ?? null;
        this.regionID = defaults.regionID ?? null;
        this.description = defaults.description ?? null;
        this.categoryID = defaults.categoryID ?? null;
        this.customData = defaults.customData ?? {};
        this.sortOrder = defaults.sortOrder ?? 10;
    }

    /**
     * buildAssociationFromData
     * @param {*} region - RegionInfo object
     * @param {*} data - Expected Structure:
     * {
     *  id?
     *  type?
     *  pack?
     *  name?
     *  transferType?
     *  sourceRegionID?
     *  object?
     *  data.thumb?
     * }
     * @param {*} categoryID 
     * @returns 
     */
    static async buildAssociationFromData(region, data, categoryID) {
        Logger.debug("Building new association:", region, data, categoryID);
        let association = new RegionAssociation();
        if (data.object) {
            association.id = data.object?.id ?? data.object.id;
        }
        association.foundryID = isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10") ? data.uuid.replace(`${data.type}.`, "") : data.object?.foundryID ?? data.id;
        association.type = data.type ?? data.transferType;
        association.compendiumKey = data.pack;
        association.regionID = region.id;

        let tabName = null;
        let pack = null;
        let isExisting = false;
        if (data.pack) {
            pack = game.packs.get(data.pack);
        }

        let associations = duplicate(region.associations);
        let existingAssociation = associations.find(a => a.foundryID === association.foundryID && a.type === association.type);
        switch (association.type) {
            case REGIONS_CONFIG.TYPES.SCENE:
                tabName = "scenes";
                let s = pack ? await pack.getDocument(data.id) : game.scenes.get(association.foundryID);
                association.foundryIMG = (isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10") ? s.thumb : s.data.thumb) ?? "icons/fvtt.png",
                association.foundryName = s.name;
                break;
            case REGIONS_CONFIG.TYPES.ACTOR:
                tabName = "actors";
                let a = pack ? await pack.getDocument(data.id) : game.actors.get(association.foundryID);
                association.foundryIMG = a.img;
                association.foundryName = a.name;
                break;
            case REGIONS_CONFIG.TYPES.ITEM:
                tabName = "items";
                let i = pack ? await pack.getDocument(data.id) : game.items.get(association.foundryID);
                association.foundryIMG = i.img;
                association.foundryName = i.name;
                break;
            case REGIONS_CONFIG.TYPES.JOURNAL:
                tabName = "journals";
                let j = pack ? await pack.getDocument(data.id) : game.journal.get(association.foundryID);
                association.foundryIMG = (isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10") ? j.img : j.data.img) ?? "icons/svg/book.svg";
                association.foundryName = j.name;
                break;
            case REGIONS_CONFIG.TYPES.ROLLTABLE:
                tabName = "rolltables";
                let t = pack ? await pack.getDocument(data.id) : game.tables.get(association.foundryID);
                association.foundryIMG = (isNewerVersion(REGIONS_CONFIG.FOUNDRYVERSION, "10") ? t.img : t.data.img) ?? "icons/vtt.png";;
                association.foundryName = t.name;
                break;
            case REGIONS_CONFIG.TYPES.PLAYLIST:
                tabName = "playlists";
                let p = pack ? await pack.getDocument(data.id) : game.playlists.get(association.foundryID);
                association.foundryIMG = "icons/svg/sound.svg";
                association.foundryName = p.name;
                break;
            case REGIONS_CONFIG.TYPES.REGION:
                tabName = "regions";
                if (data.sourceRegionID) {
                    let r = await RegionsData.getRegion(data.sourceRegionID);
                    association.foundryID = data.sourceRegionID;
                    association.foundryIMG = r.icon;
                    association.foundryName = r.name;
                } else {
                    association = data.object;
                }
                break;
            case REGIONS_CONFIG.TYPES.CUSTOM:
                tabName = "custom";
                existingAssociation = associations.find(a => a.type === REGIONS_CONFIG.TYPES.CUSTOM && a.id === data.object?.id);
                if (!existingAssociation) {
                    association.customData = data.object.customData ? duplicate(data.object.customData) : duplicate(data.object);
                    association.customData.id = association.id = foundry.utils.randomID(16);
                }
                break;
            default:
                break;
        }

        if (existingAssociation) {
            if (existingAssociation.categoryID !== categoryID) {
                association = duplicate(existingAssociation);
                association.categoryID = categoryID;
            }
            isExisting = true;
        } else {
            association.categoryID = categoryID;
        }

        return { association, tabName, isExisting: isExisting };
    }

    async getAssociationDocument() {
        let doc = null;

        if (this.compendiumKey) {
            const pack = game.packs.get(this.compendiumKey);
            doc = await pack.getDocument(this.foundryID);
        } else {
            switch (this.type) {
                case REGIONS_CONFIG.TYPES.SCENE:
                    doc = game.scenes.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.ACTOR:
                    doc = game.actors.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.ITEM:
                    doc = game.items.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.JOURNAL:
                    doc = game.journal.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.ROLLTABLE:
                    doc = game.tables.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.PLAYLIST:
                    doc = game.playlists.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.REGION:
                    doc = await RegionsData.getRegion(this.foundryID);
                    break;
                default:
                    break;
            }
        }

        return doc;
    }

    async renderAssociationSheet() {
        if (this.type === REGIONS_CONFIG.TYPES.REGION) {
            this.viewRegion();
            return;
        }
        
        let doc = await this.getAssociationDocument();
        let sheet = doc?.sheet;
        sheet?.render(true);
    }

    async viewScene() {
        if (this.type !== REGIONS_CONFIG.TYPES.SCENE) {
            return;
        }

        let scene = await this.getAssociationDocument();
        let region = await RegionsData.getRegion(this.regionID);

        if (this.compendiumKey) {
            //Check if this scene has been imported already
            let pack = game.packs.get(this.compendiumKey);

            let sceneCheck = game.scenes.getName(this.foundryName);
            if (!sceneCheck) {
                scene = await game.scenes.importFromCompendium(pack, this.foundryID, {folder: region.folders.sceneFolder});
            } else {
                scene = sceneCheck;
            }
        }

        let activateScene = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_scene_behavior);
        if (activateScene) {
            scene.activate();
        } else {
            scene.view();
        }
    }

    async viewRegion() {
        if (this.type !== REGIONS_CONFIG.TYPES.REGION) {
            return;
        }

        let region = await this.getAssociationDocument();
        region.renderRegion();
    }

    async rollTable() {
        if (this.type !== REGIONS_CONFIG.TYPES.ROLLTABLE) {
            return;
        }

        let table = await this.getAssociationDocument();
        table?.draw();
    }

    async play() {
        if (this.type !== REGIONS_CONFIG.TYPES.PLAYLIST) {
            return;
        }

        let playlist = await this.getAssociationDocument();
        playlist?.playAll();
    }

    async hasPermission() {
        let doc = null;
        if (game.user.isGM) { return true; }
        if (this.compendiumKey) {
            let pack = game.packs.get(this.compendiumKey);
            if (pack.private) { return false; }
        }
        if (this.type === REGIONS_CONFIG.TYPES.REGION) {
            let r = RegionsData.getAllRegions()?.find(r => r.id === this.foundryID);
            return (!r) ? false : !r.private;
        }

        doc = await this.getAssociationDocument();
        if (doc) {
            return doc.permission > 0;
        } else {
            return false;
        }
    }
}