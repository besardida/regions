export class ExtendedContextMenu extends ContextMenu {
    constructor (element, selector, menuItems, {eventName="contextmenu"}={}) {
        super(element, selector, menuItems, {eventName});
    }

    render(target) {
        let html = $("#context-menu").length ? $("#context-menu") : $('<nav id="context-menu"></nav>');
        let ol = $('<ol class="context-items"></ol>');
        html.html(ol);
    
        // Build menu items
        for (let item of this.menuItems) {
    
          // Determine menu item visibility (display unless false)
          let display = true;
          if ( item.condition !== undefined ) {
            display = ( item.condition instanceof Function ) ? item.condition(target) : item.condition;
          }
          if ( !display ) continue;
    
          // Construct and add the menu item
          ol.append(this.generateSubContextMenu(item, target));
        }
    
        // Bail out if there are no children
        if ( ol.children().length === 0 ) return;
    
        // Append to target
        this._setPosition(html, target);
    
        // Animate open the menu
        return this._animateOpen(html);        
    }

    generateSubContextMenu(menuItem, target, showIcon = true) {
        let name = game.i18n.localize(menuItem.name);
        let submenu = menuItem.items?.length ? `<i style="float: right; padding: 9px 4px;" class="fas fa-chevron-right"></i>` : "";
        let icon = showIcon ? `${menuItem.icon}` : "";
        let newClasses = menuItem.classes ? menuItem.classes.reduce((a, b) => `${a} ${b}`) : "";
        let li = $(`<li class="context-item"><span class="context-container ${newClasses}">${icon}<span>${name}</span></span>${submenu}</li>`);
        li.children("i").addClass("fa-fw");
        if (menuItem.items?.length) {
            let ul = $('<ul class="context-items extendedSubmenu"></ul>');
            
            for (let item of menuItem.items) {
                let display = true;
                if ( item.condition !== undefined ) {
                  display = ( item.condition instanceof Function ) ? item.condition(target) : item.condition;
                }
                if ( !display ) continue;
    
                ul.append(this.generateSubContextMenu(item, target, false));
            }

            li.append(ul);
        }

        li.click(e => {
            e.preventDefault();
            e.stopPropagation();
            menuItem.callback(target);
            this.close();
        });

        return li;
    }
}