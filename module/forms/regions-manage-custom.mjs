import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { RegionsEditCustomDialog } from "./regions-edit-custom.mjs";
import { RegionTransferData } from "../models/region-transfer-data.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";

export class RegionsManageCustomDialog extends HelpFormApplication {
    #hookIndexes = {
        "regionsUpdateCustomTags": null,
        "closeApplication": null
    };

    constructor(object, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Custom-Tags";
        super(object, options);

        this.customTags = RegionsData.getCustomTags();
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: "auto",
            width: 550,
            submitOnChange: true,
            id: "regionsManageCustomTags",
            dragDrop: [{ dropSelector: null, dragSelector: '.frDraggable' }],
            template: REGIONS_CONFIG.TEMPLATES.MANAGE_CUSTOM,
            scrollY: ['.regionsTableList'],
            title: game.i18n.localize("REGIONS.CUSTOM_TAG.Manage")
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsUpdateCustomTags = Hooks.on("regionsUpdateCustomTags", this._forceUpdateFromHook.bind(this));
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
    }

    getData(options) {
        return {
            customTags: this.customTags
        }
    }

    async _updateObject(event, formData) {

    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        const associationID = clickedElement.parents('[data-detail-id]')?.data()?.detailId;

        let dialog = null;
        let needRefresh = false;
        let _this = this;
        let customTag = null;

        switch (action) {
            case 'clone-custom':
                customTag = this.customTags.find(c => c.id === associationID);
                let newTag = duplicate(customTag);
                newTag.id = foundry.utils.randomID(16);
                newTag.name += " (Copy)";
                this.onCustomTagClose(newTag);
                break;
            case 'create':
                dialog = new RegionsEditCustomDialog();
                dialog.onClose = (customTag) => { _this.onCustomTagClose(customTag); };
                dialog.render(true);
                break;
            case 'edit-custom':
                customTag = this.customTags.find(c => c.id === associationID);
                dialog = new RegionsEditCustomDialog(customTag);
                dialog.onClose = (customTag) => { _this.onCustomTagClose(customTag); };
                dialog.render(true);
                break;
            case 'delete-custom':
                this.customTags = this.customTags.filter(c => c.id !== associationID);
                await RegionsData.updateCustomTags(this.customTags);
                needRefresh = true;
                break;
            case 'cancel':
            case 'submit':
                this.close();
                break;
            default:
                break;
        }

        if (needRefresh) {
            this.render(true);
        }

        return true;
    }

    async _forceUpdateFromHook(customTags) {
        this.customTags = RegionsData.getCustomTags();
        this.render(true);
    }

    async onCustomTagClose(customTag) {
        let tag = this.customTags.find(c => c.id === customTag.id);
        if (tag) {
            tag = customTag;
        } else {
            this.customTags.push(customTag);
        }

        await RegionsData.updateCustomTags(this.customTags);
        this.render(true);
    }

    async _onDrop(event) {
        if (!game.user.isGM) { return; }

        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData("text/plain"));
        } catch (err) {
            return false;
        }

        //If this isn't a drop from Regions, isn't a custom tag, or if it's from this same window do nothing.
        if (!data || !data?.object) { return; }
        if (data.transferType !== REGIONS_CONFIG.TYPES.CUSTOM) { return; }
        if (!data.object) { return; }
        if (data.internalID && this.customTags.find(c => c.id === data.internalID)) { return; }

        let customTag = data.object.customData ? data.object.customData : data.object;

        //Moving a custom tag to global!  Reset values parameters
        customTag.value = "";
        customTag.regionID = null;
        customTag.categoryID = null;

        if (customTag.steps) {
            for (let step of customTag.steps) { step = false; }
        }

        if (customTag.globalID && this.customTags.find(c => c.id === customTag.globalID)) {
            //This tag already exists here; make a clone
            let existingTag = this.customTags.find(c => c.id === customTag.globalID);
            if (customTag.name.toLowerCase() === existingTag.name.toLowerCase()) { customTag.name += " (Copy)"; }
            customTag.id = foundry.utils.randomID(16);
            customTag.globalID = null;
        }

        await this.onCustomTagClose(customTag);
    }

    async _onDragStart(event) {
        let detailID = $(event.target).closest("[data-detail-id]")[0]?.dataset?.detailId;
        let customTag = duplicate(this.customTags.find(c => c.id === detailID));

        customTag.globalID = detailID;

        let transferData = new RegionTransferData({
            transferType: REGIONS_CONFIG.TYPES.CUSTOM,
            internalID: customTag.id,
            object: customTag
        });

        //Update the eventdata
        event.dataTransfer.setData("text/plain", JSON.stringify(transferData));
    }
}