import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { RegionAssociation } from "./region-association.mjs";
import { RegionsEditRegionDialog } from "../forms/regions-edit-region.mjs";
import { RegionsTemplate } from "./region-template.mjs";
import { Logger } from "../logger/logger.mjs";

/**
 * @typedef {Object} RegionInfo
 * @property {string} name - The name of this region
 * @property {string} icon - The icon for this region
 * @property {string} id - Internal id of this region
 * @property {array} associations - array of items associated with this region
 * @property {Object} folders - Object of folders associated with this region
 * @property {Boolean} private - Is this region private, or can players see items within it that they have permission to see
 * @property {string} categoryID - The id of the category this region belongs to
 * @property {RegionsTemplate} template - The template used for the region
 */
export class RegionInfo {
    constructor() {
        this.name = null;
        this.notes = null;
        this.icon = "icons/tools/navigation/map-marked-blue.webp";
        this.id = foundry.utils.randomID(16);
        this.associations = [];
        this.folders = {
            sceneFolder: null,
            actorFolder: null,
            journalFolder: null
        };
        this.private = true;
        this.categoryID = null;
        this.template = null;
    }

    static async init(regionID) {
        let r = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data).find(r => r.id === regionID);
        Logger.debug("Init Region, found region:", r);
        
        let region = new RegionInfo();
        if (r) {
            Object.assign(region, r);
            if (!r.hasOwnProperty("private")) { region.private = false; }
        } else {
            Logger.error(`${game.i18n.localize("REGIONS.ERRORS.does-not-exist")} ${regionID}`);
            return;
        }

        let associations = RegionsData.getRegionAssociations(regionID);
        let validAssociations = [];
        for (let a of associations) {
            let assoc = new RegionAssociation(a);
            let hasPermission = await assoc.hasPermission();
            if (hasPermission) { validAssociations.push(assoc); }
        }

        region.associations = validAssociations;

        Logger.debug("Init Region: ", region);

        return region;
    }

    filterAndSortAssociationsByType(associationType) {
        return this.associations?.filter(a => a?.type === associationType).sort((a, b) => {
            let n1 = a.foundryName ?? a.key;
            let n2 = b.foundryName ?? b.key;
            if (!n1 || !n2) {
                return 0;
            }

            n1 = n1.toLowerCase();
            n2 = n2.toLowerCase();

            if (n1 < n2) {
                return -1;
            } else if (n1 == n2) {
                return 0;
            }

            return 1;
        });
    }

    buildRegionContextMenu() {
        let _this = this;
        let contextMenu = {
            name: this.name ?? game.i18n.localize("REGIONS.new-region"),
            icon: `<img src="${this.icon}" />`,
            items: [],
            callback: async (c) => { 
                let region = await RegionInfo.init(_this.id);
                region.renderRegion();
            }
        };

        let scenes = this.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.SCENE).map((a) => {
            let _a = a;
            return {
                name: _a.foundryName,
                callback: async (c) => {
                    let region = await RegionInfo.init(_a.regionID);
                    let association = region.getAssociationByID(_a.id);
                    await association.viewScene();
                }
            }
        });

        let actors = this.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.ACTOR).map((a) => {
            let _a = a;
            return {
                name: _a.foundryName,
                callback: async (c) => {
                    let region = await RegionInfo.init(_a.regionID);
                    let association = region.getAssociationByID(_a.id);
                    await association.renderAssociationSheet();
                }
            }
        });

        let items = this.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.ITEM).map((a) => {
            let _a = a;
            return {
                name: _a.foundryName,
                callback: async (c) => {
                    let region = await RegionInfo.init(_a.regionID);
                    let association = region.getAssociationByID(_a.id);
                    await association.renderAssociationSheet();
                }
            }
        });

        let journals = this.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.JOURNAL).map((a) => {
            let _a = a;
            return {
                name: _a.foundryName,
                callback: async (c) => {
                    let region = await RegionInfo.init(_a.regionID);
                    let association = region.getAssociationByID(_a.id);
                    await association.renderAssociationSheet();
                }
            }
        });

        let rolltables = this.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.ROLLTABLE).map((a) => {
            let _a = a;
            return {
                name: _a.foundryName,
                callback: async (c) => {
                    let region = await RegionInfo.init(_a.regionID);
                    let association = region.getAssociationByID(_a.id);
                    await association.rollTable();
                }
            }
        });

        let playlists = this.filterAndSortAssociationsByType(REGIONS_CONFIG.TYPES.PLAYLIST).map((a) => {
            let _a = a;
            return {
                name: _a.foundryName,
                callback: async (c) => {
                    let region = await RegionInfo.init(_a.regionID);
                    let association = region.getAssociationByID(_a.id);
                    await association.play();
                }
            }
        });

        if (scenes.length) {
            contextMenu.items.push({
                name: game.i18n.localize("REGIONS.scenes"),
                items: scenes,
                callback: async(c) => {
                    let region = await RegionInfo.init(_this.id);
                    region.renderRegion();
                }
            });
        }

        if (actors.length) {
            contextMenu.items.push({
                name: game.i18n.localize("REGIONS.actors"),
                items: actors,
                callback: async(c) => {
                    let region = await RegionInfo.init(_this.id);
                    region.renderRegion();
                }
            });
        }

        if (items.length) {
            contextMenu.items.push({
                name: game.i18n.localize("REGIONS.items"),
                items: items,
                callback: async(c) => {
                    let region = await RegionInfo.init(_this.id);
                    region.renderRegion();
                }
            });
        }

        if (journals.length) {
            contextMenu.items.push({
                name: game.i18n.localize("REGIONS.journals"),
                items: journals,
                callback: async(c) => {
                    let region = await RegionInfo.init(_this.id);
                    region.renderRegion();
                }
            });
        }

        if (rolltables.length) {
            contextMenu.items.push({
                name: game.i18n.localize("REGIONS.rolltables"),
                items: rolltables,
                callback: async(c) => {
                    let region = await RegionInfo.init(_this.id);
                    region.renderRegion();
                }
            });
        }

        if (playlists.length) {
            contextMenu.items.push({
                name: game.i18n.localize("REGIONS.playlists"),
                items: playlists,
                callback: async(c) => {
                    let region = await RegionInfo.init(_this.id);
                    region.renderRegion();
                }
            });
        }

        return contextMenu;
    }

    getAssociationByID(_id) {
        let association = new RegionAssociation(this.associations.find(a => a.id === _id));
        return association;
    }

    renderRegion() {
        let regionEditDialog = new RegionsEditRegionDialog({
            region: this
        });
        regionEditDialog.render(true);
    }

    get hasPermission() {
        return game.user.isGM || !this.private;
    }

    get canDelete() {
        return game.user.isGM;
    }

    async applyTemplate(template) {
        let fApply = async (context) => {
            context.template = template;
            context.categoryID = template.categoryID;
            context.icon = context.icon === "icons/tools/navigation/map-marked-blue.webp" ? template.icon : context.icon;
            for (let tag of template.customTags) {
                tag.globalID = tag.id;
                let association = new RegionAssociation({regionID: context.id, customData: tag, categoryID: tag.categoryID});
                context.associations.push(association);
            }

            await RegionsData.updateRegion(context);
        }
        let fReplace = async(context) => {
            let oldTemplate = duplicate(context.template);
            let templateIDs = oldTemplate.customTags.map((ct) => { return ct.globalID ?? ct.id; });
            context.associations = context.associations.filter(a => a.type !== 'Custom' || (a.type === 'Custom' && templateIDs.indexOf(a.customData.globalID) === -1));            
            context.template = template;
            context.icon = context.icon === "icons/tools/navigation/map-marked-blue.webp" ? template.icon : context.icon;
            for (let tag of template.customTags) {
                tag.globalID = tag.id;
                let association = new RegionAssociation({regionID: context.id, customData: tag, categoryID: tag.categoryID});
                context.associations.push(association);
            }

            await RegionsData.updateRegion(context);
        }
        let buttons = {};
        let content = "";
        let title = game.i18n.localize("REGIONS.TEMPLATE.ApplyTitle") + " " + this.name;
        let _this = this;

        if (this.template) {
            content = game.i18n.localize("REGIONS.TEMPLATE.ApplyOrReplaceToRegion");
            buttons = {
                apply: {
                    icon: "<i class='fas fa-check'></i>",
                    label: game.i18n.localize("REGIONS.TEMPLATE.Apply"),
                    callback: (html) => {
                        fApply(_this);
                    }
                },
                replace: {
                    icon: "<i class='fas fa-exchange-alt'></i>",
                    label: game.i18n.localize("REGIONS.TEMPLATE.Replace"),
                    callback: (html) => {
                        fReplace(_this);
                    }
                },
                no: {
                    icon: "<i class='fas fa-times'></i>",
                    label: game.i18n.localize("REGIONS.cancel")
                }
            }
        } else {
            content = game.i18n.localize("REGIONS.TEMPLATE.ApplyToRegion");
            buttons = {
                apply: {
                    icon: "<i class='fas fa-check'></i>",
                    label: game.i18n.localize("REGIONS.TEMPLATE.Apply"),
                    callback: (html) => {
                        fApply(_this);
                    }
                },
                no: {
                    icon: "<i class='fas fa-times'></i>",
                    label: game.i18n.localize("REGIONS.cancel")
                }
            }
        }
        new Dialog({
            title,
            content,
            buttons,
            default: "no"
        }).render(true);
    }
}
