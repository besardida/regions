import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionTransferData } from "../models/region-transfer-data.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { RegionsEditTemplateDialog } from "./regions-edit-template.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";

export class RegionsManageTemplatesDialog extends HelpFormApplication {
    #hookIndexes = {
        "regionsCreateTemplate": null,
        "regionsDeleteTemplate": null,
        "regionsUpdateTemplate": null,
        "closeApplication": null
    }

    constructor(object, options) {
        if (!object) { object = {}; }
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Templates";
        super(object, options);

        this.templates = RegionsData.getTemplates();
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            id: "regionsManageTemplatesDialog",
            closeOnSubmit: false,
            height: "auto",
            width: 550,
            scrollY: [
                ".regionsTableList"
            ],
            submitOnChange: true,
            dragDrop: [{ dropSelector: null, dragSelector: '.frDraggable' }],
            template: REGIONS_CONFIG.TEMPLATES.MANAGE_TEMPLATES,
            title: game.i18n.localize("REGIONS.TEMPLATE.Manage")
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });

            this.#hookIndexes.regionsCreateTemplate = Hooks.on("regionsCreateTemplate", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteTemplate = Hooks.on("regionsDeleteTemplate", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsUpdateTemplate = Hooks.on("regionsUpdateTemplate", this._forceUpdateFromHook.bind(this));
        }
    }

    getData(options) {
        return {
            templates: this.templates
        };
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
    }

    async _updateObject(event, formData) {

    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        const detailID = clickedElement.parents('[data-detail-id]')?.data()?.detailId;
        let template = null;

        let dialog = null;
        let _this = this;

        switch (action) {
            case 'create':
                dialog = new RegionsEditTemplateDialog();
                dialog.render(true);
                break;
            case "clone-template":
                template = duplicate(RegionsData.getTemplate(detailID));
                template.id = foundry.utils.randomID(16);
                template.name += " (Copy)";
                dialog = new RegionsEditTemplateDialog({template, isNew: true });
                dialog.render(true);
                break;
            case "delete-template":
                template = RegionsData.getTemplate(detailID);
                let tName = template.name;
                let content = `${game.i18n.localize("REGIONS.delete-template-confirm")} ${tName}?`;
                let fDelete = async () => {
                    await RegionsData.deleteTemplate(detailID);
                }
                new Dialog({
                    title: game.i18n.localize("REGIONS.delete-template"),
                    content: content,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("REGIONS.delete-template"),
                            callback: (html) => {
                                fDelete();
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case 'edit-template':
                template = RegionsData.getTemplate(detailID);
                dialog = new RegionsEditTemplateDialog({template});
                dialog.render(true);
                break;
            case "submit":
                this.close();
                break;
            default:
                break;
        }
    }

    async _forceUpdateFromHook(object) {
        this.templates = RegionsData.getTemplates(); 
        this.render(true);
    }

    async _onDragStart(event) {
        let templateID = $(event.target).closest("[data-detail-id]")[0]?.dataset?.detailId;
        let template = this.templates.find(t => t.id === templateID);

        let transferData = new RegionTransferData({
            transferType: REGIONS_CONFIG.TYPES.TEMPLATE,
            internalID: templateID,
            object: template
        });

        //Update the eventdata
        event.dataTransfer.setData("text/plain", JSON.stringify(transferData));
    }
}