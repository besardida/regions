import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { Logger } from "../logger/logger.mjs";
import { RegionInfo } from "./region-info.mjs";

export const RegionsBackupTypes = {
    CUSTOMTAG: "CustomTag",
    GLOBAL: "Global",
    REGION: "Region",
    TEMPLATE: "Template"
}

export class RegionsBackup {
    id = null;
    backupDate = null;
    regionsVersion = null;
    backupType = null;
    worldID = null;
    
    constructor() {
        this.id = foundry.utils.randomID(16);
        this.backupDate = new Date().toLocaleString();
        this.worldID = game.world.id;
        this.regionsVersion = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_version);
    }
}

export class RegionsGlobalBackup extends RegionsBackup {
    regions = null;
    categories = null;
    documents = null;

    constructor() {
        super();
        this.backupType = RegionsBackupTypes.GLOBAL;
        this.categories = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.regions_category);
        this.regions = RegionsData.getAllRegions();
        this.documents = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_documents);
    }
}

export class RegionsRegionBackup extends RegionsBackup {
    region = null;
    categories = [];

    constructor(region) {
        super();
        this.backupType = RegionsBackupTypes.REGION;
        this.region = region;

        let rCats = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
        let aCats = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);

        if (this.region.categoryID) {
            this.categories = this.categories.concat(rCats.find(r => r.id === this.region.categoryID));
        }

        for (let a of aCats) {
            if (!a) { continue; }
            let toAdd = this.region.associations.find(r => r.categoryID === a.id);
            if (toAdd) { this.categories = this.categories.concat(a); }
        }
    }

    static async CreateRegionBackupForRegionID(regionID) {
        let region = await RegionInfo.init(regionID);
        return new RegionsRegionBackup(region);
    }
}

export class RegionsTemplateBackup extends RegionsBackup {
    template = null;
    categories = [];

    constructor(templateID) {
        super();
        this.backupType = RegionsBackupTypes.TEMPLATE;
        this.template = RegionsData.getTemplate(templateID);

        let rCats = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
        let aCats = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.ASSOCIATION);

        if (this.template.categoryID) {
            this.categories.push(rCats.find(c => c.id === this.template.categoryID));
        }

        for (let cat of aCats) {
            if (!cat) { continue; }
            let toAdd = this.template.customTags.find(t => t.categoryID === cat.id);
            if (toAdd) { this.categories = this.categories.concat(cat); }
        }
    }
}

export class RegionsCustomTagBackup extends RegionsBackup {
    customTags = [];

    constructor(customTagID = null) {
        super();
        this.backupType = RegionsBackupTypes.CUSTOMTAG;

        this.customTags = RegionsData.getCustomTags();
        if (customTagID) {
            this.customTags = this.customTags.filter(c => c.id === customTagID);
        }
    }
}