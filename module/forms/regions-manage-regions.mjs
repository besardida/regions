import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { RegionsEditRegionDialog } from "./regions-edit-region.mjs";
import { RegionsManageCustomDialog } from "./regions-manage-custom.mjs";
import { RegionsManageTemplatesDialog } from "./regions-manage-templates.mjs";
import { RegionsManageCategoriesDialog } from "./regions-manage-categories.mjs";
import { RegionTransferData } from "../models/region-transfer-data.mjs";
import { HelpFormApplication } from "../about/help-form-application.mjs";
import { RegionsBackupManagerApplication } from "./regions-backup-manager.mjs";

/**
 * Custom implementation to review all Foundry Regions
 */
export class RegionsManageRegionsDialog extends HelpFormApplication {
    #hookIndexes = {
        "regionsUpdateCategory": null,
        "regionsCreateCategory": null,
        "regionsDeleteCategory": null,
        "regionsUpdateRegion": null,
        "regionsDeleteRegion": null,
        "regionsCreateRegion": null,
        "closeApplication": null
    };

    constructor(object, options) {
        if (!object) { object = {}; }
        object.enableAboutButton = true;
        object.wikiLink = "https://gitlab.com/geekswordsman/regions/-/wikis/Manage-Regions";
        super(object, options);
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: 'auto',
            width: '800',
            id: 'regionsOverview',
            classes: ['regions'],
            submitOnChange: true,
            dragDrop: [{ dropSelector: null, dragSelector: '.regionsDraggable' }],
            scrollY: [
                ".regionsOverviewList"
            ],
            template: REGIONS_CONFIG.TEMPLATES.REGIONS,
            title: game.i18n.localize("REGIONS.regions-overview")
        }

        const mergedOptions = foundry.utils.mergeObject(defaults, overrides);

        return mergedOptions;
    }

    async render(force, options) {
        super.render(force, options);

        if (this.#hookIndexes.closeApplication === null) {
            this.#hookIndexes.closeApplication = Hooks.on("closeApplication", (dialog, window) => {
                if (dialog.appId === this.appId) {
                    for (let key of Object.keys(this.#hookIndexes)) {
                        Hooks.off(key, this.#hookIndexes[key]);
                    }
                }
            });
    
            this.#hookIndexes.regionsUpdateCategory = Hooks.on("regionsUpdateCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsCreateCategory = Hooks.on("regionsCreateCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteCategory = Hooks.on("regionsDeleteCategory", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsUpdateRegion = Hooks.on("regionsUpdateRegion", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsDeleteRegion = Hooks.on("regionsDeleteRegion", this._forceUpdateFromHook.bind(this));
            this.#hookIndexes.regionsCreateRegion = Hooks.on("regionsCreateRegion", this._forceUpdateFromHook.bind(this));
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', "[data-action]", this._handleButtonClick.bind(this));
    }

    getData(options) {
        let regions = RegionsData.getAllRegions().sort((a, b) => { if (a.category < b.category) { return -1; } else if (a.category > b.category) { return 1; } return 0; });
        let categories = RegionsData.getCategories(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
        let data = {
            canCreate: game.user.isGM,
            regions: regions.length > 0,
            category: []
        };

        for (let c of categories) {
            let r = regions.filter(region => region.categoryID === c.id);
            data.category.push({
                id: c.id,
                readonly: !data.canCreate,
                name: c === undefined ? "All" : c.name,
                regions: r
            });
        }

        data.category.push({
            id: null,
            readonly: true,
            name: game.i18n.localize("REGIONS.uncategorized"),
            regions: regions.filter(r => !r.categoryID)
        });

        return data;
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        let regionID = clickedElement.data().regionId ?? clickedElement.parents('[data-region-id]')?.data()?.regionId;
        let categoryID = clickedElement.data().categoryId ?? clickedElement.parents('[data-category-id]')?.data()?.categoryId;

        let _this = this;
        let region = null;

        switch (action) {
            case 'create-category':
                await RegionsData.createCategory(REGIONS_CONFIG.CATEGORY_TYPES.REGION);
                this.render();
                break;
            case 'manage-categories':
                this._manageCategories();
                break;
            case 'manage-templates':
                this._manageTemplates();
                break;
            case 'manage-custom-tags':
                this._manageCustomTags();
                break;
            case 'create-region':
                region = await RegionsData.createRegion();
                this.render();
                regionID = region.id;
            case 'edit':
                region = await RegionsData.getRegion(regionID);
                let regionEditDialog = new RegionsEditRegionDialog({
                    region: region,
                    onSaveCallback: () => {
                        _this.render();
                    }
                });
                regionEditDialog.render(true);
                break;
            case 'delete':
                region = await RegionsData.getRegion(regionID);
                let rName = region.name ?? game.i18n.localize("REGIONS.default-name");
                let content = `${game.i18n.localize("REGIONS.delete-region-confirm")} ${rName}?`;
                let fDelete = async () => {
                    await RegionsData.deleteRegion(regionID);
                    this.render();
                }
                new Dialog({
                    title: game.i18n.localize("REGIONS.delete-region"),
                    content: content,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("REGIONS.delete-region"),
                            callback: (html) => {
                                fDelete();
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("REGIONS.cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case "delete-category":
                let regions = RegionsData.getAllRegions();
                let affectedRegions = regions.filter(r => r.categoryID === categoryID);
                for(let r of affectedRegions) {
                    r.categoryID = null;
                }

                await game.settings.set(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_data, regions);
                this.render(true);
                break;
            case "toggle-collapse":
                let contentDiv = clickedElement.parents('.regionsCategoryHeader').siblings('.regionsCategoryContent');
                let icon = clickedElement.children('.fas');

                contentDiv.toggleClass('open');
                contentDiv.slideToggle();
                icon.toggleClass('fa-caret-square-down');
                icon.toggleClass('fa-caret-square-up');
                break;
            default:
                break;
        }
    }

    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        buttons.unshift({
            label: game.i18n.localize("REGIONS.BACKUPS.Manage"),
            class: "regionsManageBackupsButton",
            title: game.i18n.localize("REGIONS.BACKUPS.Manage"),
            icon: "fas fa-file-export",
            onclick: () => { this._manageBackups(); }
        })

        return buttons;
    }

    async _updateObject(event, formData) {
        const categoryID = event.target.dataset.categoryId;
        const value = event.target.value;

        let category = RegionsData.getCategory(categoryID);
        category.name = value;

        await RegionsData.updateCategory(category);
    }

    _manageBackups() {
        let dialog = new RegionsBackupManagerApplication();
        dialog.render(true);
    }

    _manageCustomTags() {
        let dialog = new RegionsManageCustomDialog();
        dialog.render(true);
    }

    _manageTemplates() {
        let dialog = new RegionsManageTemplatesDialog();
        dialog.render(true);
    }

    _manageCategories() {
        let dialog = new RegionsManageCategoriesDialog();
        dialog.render(true);
    }

    _forceUpdateFromHook = (category) => {
        this.render(true);
    }

    async _onDragStart(event) {
        if (!game.user.isGM) { return; }

        let transferData = new RegionTransferData({
            sourceRegionID: $(event.target).closest("[data-region-id]")[0]?.dataset?.regionId,
            transferType: REGIONS_CONFIG.TYPES.REGION
        });

        event.dataTransfer.setData("text/plain", JSON.stringify(transferData));
    }

    async _onDrop(event) {
        if (!game.user.isGM) { return; }

        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData("text/plain"));
        } catch (err) {
            return false;
        }

        if (data.source === REGIONS_CONFIG.ID) {
            let region = null;

            if (data.transferType === REGIONS_CONFIG.TYPES.REGION) {
                const newCategoryID = $(event.target).closest("[data-category-id]")[0]?.dataset?.categoryId;
                region = await RegionsData.getRegion(data.sourceRegionID);
                region.categoryID = newCategoryID;
                await RegionsData.updateRegion(region);
                this.render();
            } else if (data.transferType === REGIONS_CONFIG.TYPES.TEMPLATE) {
                let template = data.object;
                let regionID = $(event.target).closest("[data-region-id]").data().regionId;
                region = await RegionsData.getRegion(regionID);
                await region.applyTemplate(template);
            }
        } 
    }
}